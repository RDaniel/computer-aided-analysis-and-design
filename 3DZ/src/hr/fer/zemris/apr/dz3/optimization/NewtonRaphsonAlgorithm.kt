package hr.fer.zemris.apr.dz3.optimization

import hr.fer.zemris.apr.dz3.decomposition.solveEquationUsingLUP
import hr.fer.zemris.apr.dz3.function.HessianFunction
import org.apache.commons.math3.linear.ArrayRealVector
import org.apache.commons.math3.linear.RealMatrix
import org.apache.commons.math3.linear.RealVector


fun newtonRaphsonAlgorithm(function: HessianFunction, startingPoint: RealVector, optimalLambda: Boolean = true, precision: Double = 10E-6): RealVector {
    var currentPoint = ArrayRealVector(startingPoint)

    var divergence = 0
    var previousBest = function.valueAt(startingPoint)

    do {
        val hessian = function.getHessianMatrix(currentPoint)
        val gradient = function.gradientValueAt(currentPoint)

        val dx = calculateDx(hessian, gradient)
        val lambda = getLambda(optimalLambda, function, currentPoint, dx)

        currentPoint = currentPoint.add(dx.mapMultiply(lambda))
        val currentValue = function.valueAt(currentPoint)
        if (currentValue < previousBest) {
            divergence = 0
            previousBest = currentValue
        } else {
            divergence++
            checkForDivergence(divergence)
        }
    } while (shouldContinue(gradient, precision))

    return currentPoint
}

fun calculateDx(hessian: RealMatrix, gradient: RealVector): RealVector {
    val negativeGradient = gradient.mapMultiply(-1.0)
    return solveEquationUsingLUP(hessian, negativeGradient)
}

private fun checkForDivergence(divergence: Int) {
    if (divergence >= DIVERGENCE_COUNT) {
        throw IllegalStateException("Algorithm diverged !")
    }
}