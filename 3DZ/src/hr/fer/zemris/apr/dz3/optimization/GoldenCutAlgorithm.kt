package hr.fer.zemris.apr.dz3.optimization

import hr.fer.zemris.apr.dz3.asVector
import hr.fer.zemris.apr.dz3.function.Function


fun goldenCutAlgorithm(startingPoint: Double, function: Function, e: Double = 10E-6): Double {
    val interval = unimodalIntervalFinder(0.5, startingPoint, function)
    return goldenCutAlgorithm(interval[0], interval[1], function, e)
}

fun goldenCutAlgorithm(left: Double, right: Double, function: Function, e: Double = 10E-6): Double {
    val k = 0.5 * (Math.sqrt(5.0) - 1)

    var a = left
    var b = right
    var c = b - k * (b - a)
    var d = a + k * (b - a)

    var fa = function.valueAt(a.asVector())
    var fb = function.valueAt(b.asVector())
    var fc = function.valueAt(c.asVector())
    var fd = function.valueAt(d.asVector())

    var step = 0

    printValues(step, a, b, c, d, fa, fb, fc, fd)

    step = 1

    while ((b - a) > e) {
        if (fc < fd) {
            b = d
            d = c
            c = b - k * (b - a)
            fb = fd
            fd = fc
            fc = function.valueAt(c.asVector())
        } else {
            a = c
            c = d
            d = a + k * (b - a)
            fa = fc
            fc = fd
            fd = function.valueAt(d.asVector())
        }

        printValues(step, a, b, c, d, fa, fb, fc, fd)
        step++
    }

    return (a + b) / 2.0
}

fun unimodalIntervalFinder(shift: Double, x: Double, function: Function): Array<Double> {
    var left = x - shift
    var right = x + shift
    var m = x
    var step = 1

    var fm = function.valueAt(x.asVector())
    var fl = function.valueAt(left.asVector())
    var fr = function.valueAt(right.asVector())

    if (fm < fr && fm < fl)
        return arrayOf(left, right)
    else if (fm > fr) {
        do {
            left = m
            m = right
            fm = fr
            step *= 2
            right = x + shift * step
            fr = function.valueAt(right.asVector())
        } while (fm > fr)
    } else {
        do {
            right = m
            m = left
            fm = fl
            step *= 2
            left = x - shift * step
            fl = function.valueAt(left.asVector())
        } while (fm > fl)
    }

    return arrayOf(left, right)
}

private fun printValues(step: Int, a: Double, b: Double, c: Double, d: Double, fa: Double, fb: Double, fc: Double, fd: Double) {
    println("Iteration = $step")
    println("a=$a b=$b c=$c d=$d")
    println("f(a)=$fa f(b)=$fb f(c)=$fc f(d)=$fd")
}