package hr.fer.zemris.apr.dz3.optimization

import hr.fer.zemris.apr.dz3.function.Function
import org.apache.commons.math3.linear.ArrayRealVector
import org.apache.commons.math3.linear.RealVector


fun simplexOptimization(startingX: RealVector, function: Function, shift: Double = 1.0, alpha: Double = 1.0, beta: Double = 0.5, gamma: Double = 1.0, sigma: Double = 0.5, epsilon: Double = 10E-6): RealVector {
    val n: Int = startingX.dimension
    val simplexPoints = mutableListOf(startingX)

    for (i in 0 until n) {
        val values = startingX.copy()
        values.setEntry(i, values.getEntry(i) + shift)
        simplexPoints.add(values)
    }

    var bestPoint: Int

    do {
        val functionValues = simplexPoints.mapIndexed { index, doubles -> function.valueAt(doubles) to index }.toMap().toSortedMap()
        bestPoint = functionValues[functionValues.firstKey()]!!
        val worstPoint = functionValues[functionValues.lastKey()]!!

        val centroid = calculateCentroid(simplexPoints, worstPoint, n)

        val xr = reflection(centroid, simplexPoints[worstPoint], alpha)
        val reflectionValue = function.valueAt(xr)
        if (reflectionValue < functionValues.firstKey()) {
            val xe = expansion(centroid, simplexPoints[bestPoint], gamma)

            if (function.valueAt(xe) < functionValues.firstKey()) {
                simplexPoints[worstPoint] = xe
            } else {
                simplexPoints[worstPoint] = xr
            }
        } else {
            if (isReflectionBigger(reflectionValue, functionValues, worstPoint)) {
                if (reflectionValue < functionValues.lastKey()) {
                    simplexPoints[worstPoint] = xr
                }

                val xk = contraction(centroid, simplexPoints[worstPoint], beta)
                if (function.valueAt(xk) < functionValues.lastKey()) {
                    simplexPoints[worstPoint] = xk
                } else {
                    moveAllPointsToBestPoint(simplexPoints, bestPoint, sigma)
                }
            } else {
                simplexPoints[worstPoint] = xr
            }
        }

        println("Centroid=$centroid")
        println("f(centroid)=${function.valueAt(centroid, false)}")
    } while (checkConstraint(simplexPoints, centroid, function, epsilon))

    return simplexPoints[bestPoint]
}

fun checkConstraint(simplexPoints: List<RealVector>, centroid: RealVector, function: Function, epsilon: Double): Boolean {
    var totalSum = 0.0
    for (simplexPoint in simplexPoints) {
        totalSum += Math.pow((function.valueAt(simplexPoint) - function.valueAt(centroid)), 2.0)
    }

    totalSum /= simplexPoints.size

    return Math.sqrt(totalSum) > epsilon
}

fun moveAllPointsToBestPoint(simplexPoints: MutableList<RealVector>, bestPoint: Int, sigma: Double) {
    val size = simplexPoints.size

    for (i in 0 until size) {
        if (i == bestPoint) continue
        simplexPoints[i] = simplexPoints[i].add(simplexPoints[bestPoint]).mapMultiply(sigma)
    }
}

fun isReflectionBigger(evaluate: Double, functionValues: Map<Double, Int>, worstPoint: Int): Boolean {
    for (functionValue in functionValues) {
        if (functionValue.value == worstPoint) continue
        if (evaluate < functionValue.key) return false
    }

    return true
}

fun reflection(centroid: RealVector, worstPoint: RealVector, alpha: Double) =
    centroid.mapMultiply(1 + alpha).subtract(worstPoint.mapMultiply(alpha))

fun contraction(centroid: RealVector, worstPoint: RealVector, beta: Double) =
    centroid.mapMultiply(1 - beta).add(worstPoint.mapMultiply(beta))

fun expansion(centroid: RealVector, reflection: RealVector, gamma: Double) =
    centroid.mapMultiply(1 - gamma).add(reflection.mapMultiply(gamma))

fun calculateCentroid(simplexPoints: List<RealVector>, worstPoint: Int, dimension: Int): RealVector {
    val size = simplexPoints.size
    var centroid = ArrayRealVector(dimension)

    for (i in 0 until size) {
        if (i == worstPoint) continue
        centroid = centroid.add(simplexPoints[i])
    }

    return centroid.mapDivide(size - 1.0)
}