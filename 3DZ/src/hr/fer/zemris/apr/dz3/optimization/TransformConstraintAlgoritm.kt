package hr.fer.zemris.apr.dz3.optimization

import hr.fer.zemris.apr.dz3.constraint.ImplicitConstraint
import hr.fer.zemris.apr.dz3.function.Function
import hr.fer.zemris.apr.dz3.function.InequalityFunction
import hr.fer.zemris.apr.dz3.function.TransformConstraintFunction
import org.apache.commons.math3.linear.RealVector
import kotlin.math.absoluteValue

fun transformConstraintAlgorithm(startingX: RealVector, function: Function, startingT: Double = 1.0, equalityConstrains: List<ImplicitConstraint>, inequalityConstraints: List<ImplicitConstraint>, epsilon: Double = 10E-12): RealVector {
    var currentX = startingX
    for (inequalityConstraint in inequalityConstraints) {
        if (!inequalityConstraint.isSatisfied(startingX)) {
            val inequalityFunction = InequalityFunction(currentX, startingT, inequalityConstraints)
            currentX = simplexOptimization(startingX, inequalityFunction)
        }
    }

    var t = startingT
    var nextX = currentX

    var iterations = 0
    do {
        currentX = nextX
        val transformFunction = TransformConstraintFunction(currentX, function, t, equalityConstrains, inequalityConstraints)
        nextX = simplexOptimization(currentX, transformFunction)
        t *= 10
        iterations++
    } while (checkDistancesBetweenDimensions(currentX, nextX, epsilon))

    println(iterations)
    return currentX
}

fun checkDistancesBetweenDimensions(x1: RealVector, x2: RealVector, epsilon: Double): Boolean {
    for (i in 0 until x1.dimension) {
        if ((x1.getEntry(i).absoluteValue - x2.getEntry(i).absoluteValue).absoluteValue > epsilon) {
            return true
        }
    }

    return false
}
