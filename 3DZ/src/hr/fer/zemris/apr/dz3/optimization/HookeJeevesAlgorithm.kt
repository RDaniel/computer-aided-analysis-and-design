package hr.fer.zemris.apr.dz3.optimization

import hr.fer.zemris.apr.dz3.function.Function
import org.apache.commons.math3.linear.RealVector


fun hookeJeevesAlgorithm(x0: RealVector, function: Function, shift: Double = 3.0, epsilon: Double = 10E-6): RealVector {
    var xp = x0.copy()
    var xb = x0.copy()
    var dx = shift
    do {
        val xn = explore(xp, dx, function)
        if (function.valueAt(xn) < function.valueAt(xb)) {
            xp = xn.mapMultiply(2.0).subtract(xb)
            xb = xn
        } else {
            dx /= 2.0
            xp = xb.copy()
        }

        println(dx)
        printPoints(xb, xp, xn, function)
    } while (dx > epsilon)

    return xb
}

private fun explore(xp: RealVector, shift: Double, function: Function): RealVector {
    val x = xp.copy()
    val size = xp.dimension

    for (i in 0 until size) {
        val pValue = function.valueAt(x)
        var nValue = function.valueAt(x)

        if (nValue > pValue) {
            x.setEntry(i, x.getEntry(i) - 2 * shift)
            nValue = function.valueAt(x)

            if (nValue > pValue) {
                x.setEntry(i, x.getEntry(i) + shift)
            }
        }
    }

    return x
}

private fun printPoints(xb: RealVector, xp: RealVector, xn: RealVector, function: Function) {
    println("xb=$xb xp=$xp xn=$xn")
    println("f(xb)=${function.valueAt(xb, false)} f(xp)=${function.valueAt(xp, false)} f(xn)=${function.valueAt(xn, false)}")
}
