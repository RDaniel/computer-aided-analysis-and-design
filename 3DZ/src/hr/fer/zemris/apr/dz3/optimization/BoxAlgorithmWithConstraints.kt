package hr.fer.zemris.apr.dz3.optimization

import hr.fer.zemris.apr.dz3.constraint.Constraint
import hr.fer.zemris.apr.dz3.constraint.ExplicitConstraint
import hr.fer.zemris.apr.dz3.function.Function
import org.apache.commons.math3.linear.ArrayRealVector
import org.apache.commons.math3.linear.RealVector
import java.util.*
import kotlin.collections.ArrayList

const val C_T_MULTIPLIER = 0.5

fun boxAlgorithmWithConstraints(startingX: RealVector, function: Function, explicitConstraint: ExplicitConstraint, implicitConstraints: List<Constraint>, alpha: Double = 1.3, epsilon: Double = 10E-6): RealVector {
    for (constrain in listOf(explicitConstraint) + implicitConstraints) {
        if (!constrain.isSatisfied(startingX)) {
            throw IllegalArgumentException("Starting point doesn't satisfy constraints.")
        }
    }
    val dimension = startingX.dimension
    var centroid = startingX.copy()

    val random = Random()

    val points = mutableListOf<RealVector>()
    for (t in 0 until 2 * startingX.dimension) {
        var newVector: RealVector = ArrayRealVector(startingX.dimension)

        for (i in 0 until startingX.dimension) {
            val lower = explicitConstraint.lowerBound.getEntry(i)
            val upper = explicitConstraint.upperBound.getEntry(i)
            newVector.setEntry(i, lower + random.nextDouble() * (upper - lower))
        }

        newVector = moveInAllowedArea(implicitConstraints, newVector, centroid)

        points.add(newVector)
        centroid = calculateBoxCentroid(points, dimension)
    }

    do {
        val functionValues = points.mapIndexed { index, doubles -> function.valueAt(doubles) to index }.toMap().toSortedMap()
        val values = ArrayList<Int>(functionValues.values)
        val worstPoint = values[values.size - 1]
        val secondWorstPoint = values[values.size - 2]

        centroid = calculateBoxCentroid(points, dimension, worstPoint)

        var xr = reflection(centroid, points[worstPoint], alpha)
        for (i in 0 until dimension) {
            if (xr.getEntry(i) < explicitConstraint.lowerBound.getEntry(i)) {
                xr.setEntry(i, explicitConstraint.lowerBound.getEntry(i))
            } else if (xr.getEntry(i) > explicitConstraint.upperBound.getEntry(i)) {
                xr.setEntry(i, explicitConstraint.upperBound.getEntry(i))
            }
        }

        xr = moveInAllowedArea(implicitConstraints, xr, centroid)
        if (function.valueAt(xr) > function.valueAt(points[secondWorstPoint])) {
            xr = moveToCentroid(xr, centroid)
        }

        points[worstPoint] = xr
    } while (notConvergedToCentroid(points[worstPoint], centroid, epsilon))

    return centroid
}

fun notConvergedToCentroid(realVector: RealVector, centroid: RealVector, epsilon: Double): Boolean {
    return realVector.subtract(centroid).norm > epsilon
}

private fun moveInAllowedArea(implicitConstraints: List<Constraint>, vector: RealVector, centroid: RealVector): RealVector {
    var newVector = vector

    var notSatisfiedAll = true

    while (notSatisfiedAll) {
        notSatisfiedAll = false
        for (implicitConstraint in implicitConstraints) {
            if (!implicitConstraint.isSatisfied(newVector)) {
                notSatisfiedAll = true
                newVector = moveToCentroid(newVector, centroid)
            }
        }
    }
    return newVector
}

private fun moveToCentroid(newVector: RealVector, centroid: RealVector) = newVector.add(centroid).mapMultiply(C_T_MULTIPLIER)


fun calculateBoxCentroid(simplexPoints: List<RealVector>, dimension: Int, worstPoint: Int = -1): RealVector {
    val size = simplexPoints.size
    var centroid = ArrayRealVector(dimension)

    for (i in 0 until size) {
        if (i == worstPoint) continue
        centroid = centroid.add(simplexPoints[i])
    }

    return if (worstPoint == -1) {
        centroid.mapDivide(size.toDouble())
    } else {
        centroid.mapDivide(size - 1.0)
    }
}