package hr.fer.zemris.apr.dz3.optimization

import hr.fer.zemris.apr.dz3.function.Function
import hr.fer.zemris.apr.dz3.function.GradientFunction
import hr.fer.zemris.apr.dz3.function.LambdaWrapperFunction
import org.apache.commons.math3.linear.ArrayRealVector
import org.apache.commons.math3.linear.RealVector

const val DIVERGENCE_COUNT = 100

fun gradientDescentAlgorithm(function: GradientFunction, startingPoint: RealVector, optimalLambda: Boolean = true, precision: Double = 10E-6): RealVector {
    var currentPoint = ArrayRealVector(startingPoint)

    var divergence = 0
    var previousBest = function.valueAt(startingPoint)

    do {
        val gradient = function.gradientValueAt(currentPoint)

        val negativeGradient = gradient.mapMultiply(-1.0)
        val lambda = getLambda(optimalLambda, function, currentPoint, negativeGradient)

        currentPoint = currentPoint.add(negativeGradient.mapMultiply(lambda))
        val currentValue = function.valueAt(currentPoint)
        if (currentValue < previousBest) {
            divergence = 0
            previousBest = currentValue
        } else {
            divergence++
            checkForDivergence(divergence)
        }
    } while (shouldContinue(gradient, precision))

    return currentPoint
}

fun getLambda(optimalLambda: Boolean, function: GradientFunction, currentPoint: ArrayRealVector, negativeGradient: RealVector): Double {
    return if (optimalLambda) {
        findOptimalLambda(function, currentPoint, negativeGradient)
    } else {
        1.0
    }
}

private fun checkForDivergence(divergence: Int) {
    if (divergence >= DIVERGENCE_COUNT) {
        throw IllegalStateException("Algorithm diverged !")
    }
}

private fun findOptimalLambda(function: Function, currentPoint: RealVector, direction: RealVector): Double {
    val lambdaWrapper = LambdaWrapperFunction(currentPoint, function, direction)
    return goldenCutAlgorithm(lambdaWrapper.getStartingPoint().getEntry(0), lambdaWrapper)
}

fun shouldContinue(gradient: RealVector, precision: Double) = gradient.norm > precision