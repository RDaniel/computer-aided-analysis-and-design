package hr.fer.zemris.apr.dz3

import hr.fer.zemris.apr.dz3.function.ThirdFunction
import hr.fer.zemris.apr.dz3.optimization.gradientDescentAlgorithm

fun main(args: Array<String>) {
    var function = ThirdFunction()
    println("Without optimal lambda")
    try {
        println("Gradient descent method second function = ${gradientDescentAlgorithm(function, function.getStartingPoint(), optimalLambda = false)}")
    } catch (exp: IllegalStateException) {
        println("Algorithm diverged!")
    }
    println("Function evaluations -> ${function.getNumberOfFunctionEvaluations()}")

    function = ThirdFunction()
    println("With optimal lambda")
    println("Gradient descent method second function = ${gradientDescentAlgorithm(function, function.getStartingPoint(), optimalLambda = true)}")
    println("Function evaluations -> ${function.getNumberOfFunctionEvaluations()}")
    println("Gradient evaluations -> ${function.getNumberOfGradientEvaluations()}")
}