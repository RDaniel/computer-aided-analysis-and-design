package hr.fer.zemris.apr.dz3.function

import org.apache.commons.math3.linear.ArrayRealVector
import org.apache.commons.math3.linear.RealVector

class LambdaWrapperFunction(
    private val values: RealVector,
    private val function: Function,
    private val direction: RealVector)
    : Function {

    override fun getNumberOfFunctionEvaluations() = function.getNumberOfFunctionEvaluations()

    override fun getMinimum() = function.getMinimum()

    override fun getStartingPoint() = ArrayRealVector(doubleArrayOf(0.0))

    override fun numberOfVariables() = 1

    override fun valueAt(point: RealVector, countEvaluation: Boolean): Double {
        val valuesCopy = values.copy().add(direction.mapMultiply(point.getEntry(0)))
        return function.valueAt(valuesCopy)
    }
}