package hr.fer.zemris.apr.dz3.function

import org.apache.commons.math3.linear.RealMatrix
import org.apache.commons.math3.linear.RealVector

interface HessianFunction : GradientFunction {

    fun getNumberOfHessianEvaluations(): Int

    fun getHessianMatrix(point: RealVector): RealMatrix

}
