package hr.fer.zemris.apr.dz3.function

import hr.fer.zemris.apr.dz3.getPointValues
import org.apache.commons.math3.linear.ArrayRealVector
import org.apache.commons.math3.linear.RealVector

class FourthFunction : Function {

    private var numberOfFunctionEvaluations = 0

    override fun getNumberOfFunctionEvaluations() = numberOfFunctionEvaluations

    override fun getMinimum() = 0

    override fun getStartingPoint() = ArrayRealVector(doubleArrayOf(0.0, 0.0))

    override fun numberOfVariables() = 2

    override fun valueAt(point: RealVector, countEvaluation: Boolean): Double {
        val values = point.getPointValues(numberOfVariables())
        if (countEvaluation) numberOfFunctionEvaluations++

        return Math.pow(values[0] - 3, 2.0) + values[1] * values[1]
    }
}