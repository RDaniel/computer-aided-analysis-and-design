package hr.fer.zemris.apr.dz3.function

import hr.fer.zemris.apr.dz3.getPointValues
import org.apache.commons.math3.linear.ArrayRealVector
import org.apache.commons.math3.linear.MatrixUtils
import org.apache.commons.math3.linear.RealMatrix
import org.apache.commons.math3.linear.RealVector

class SecondFunction : HessianFunction {

    private var numberOfFunctionEvaluations = 0

    private var numberOfGradientEvaluations = 0

    private var numberOfHessianEvaluations = 0

    override fun getNumberOfFunctionEvaluations() = numberOfFunctionEvaluations

    override fun getNumberOfGradientEvaluations() = numberOfGradientEvaluations

    override fun getNumberOfHessianEvaluations() = numberOfHessianEvaluations

    override fun getMinimum() = 0

    override fun getStartingPoint() = ArrayRealVector(doubleArrayOf(0.1, 0.3))

    override fun numberOfVariables() = 2

    override fun valueAt(point: RealVector, countEvaluation: Boolean): Double {
        val values = point.getPointValues(numberOfVariables())
        if (countEvaluation) numberOfFunctionEvaluations++

        return (values[0] - 4) * (values[0] - 4) + 4 * (values[1] - 2) * (values[1] - 2)
    }

    override fun gradientValueAt(point: RealVector): RealVector {
        val values = point.getPointValues(numberOfVariables())
        numberOfGradientEvaluations++

        val dx1 = 2 * (values[0] - 4)
        val dx2 = 8 * (values[1] - 2)

        return ArrayRealVector(doubleArrayOf(dx1, dx2))
    }

    override fun getHessianMatrix(point: RealVector): RealMatrix {
        numberOfHessianEvaluations++

        val dx11 = 2.0
        val dx22 = 8.0
        val dx12 = 0.0

        return MatrixUtils.createRealMatrix(arrayOf(doubleArrayOf(dx11, dx12), doubleArrayOf(dx12, dx22)))
    }
}