package hr.fer.zemris.apr.dz3.function

import hr.fer.zemris.apr.dz3.getPointValues
import org.apache.commons.math3.linear.ArrayRealVector
import org.apache.commons.math3.linear.RealVector

class ThirdFunction : GradientFunction {

    private var numberOfFunctionEvaluations = 0

    private var numberOfGradientEvaluations = 0

    override fun getNumberOfFunctionEvaluations() = numberOfFunctionEvaluations

    override fun getNumberOfGradientEvaluations() = numberOfGradientEvaluations

    override fun getMinimum() = 0

    override fun getStartingPoint() = ArrayRealVector(doubleArrayOf(0.0, 0.0))

    override fun numberOfVariables() = 2

    override fun valueAt(point: RealVector, countEvaluation: Boolean): Double {
        val values = point.getPointValues(numberOfVariables())
        if (countEvaluation) numberOfFunctionEvaluations++

        return (values[0] - 2) * (values[0] - 2) + (values[1] + 3) * (values[1] + 3)
    }

    override fun gradientValueAt(point: RealVector): RealVector {
        val values = point.getPointValues(numberOfVariables())
        numberOfGradientEvaluations++

        val dx1 = 2 * (values[0] - 2)
        val dx2 = 2 * (values[1] + 3)

        return ArrayRealVector(doubleArrayOf(dx1, dx2))
    }
}