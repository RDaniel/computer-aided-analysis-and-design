package hr.fer.zemris.apr.dz3.function

import org.apache.commons.math3.linear.RealVector

interface GradientFunction : Function {

    fun getNumberOfGradientEvaluations(): Int

    fun gradientValueAt(point: RealVector): RealVector

}
