package hr.fer.zemris.apr.dz3.function

import hr.fer.zemris.apr.dz3.constraint.ImplicitConstraint
import org.apache.commons.math3.linear.RealVector
import kotlin.math.ln

const val WRONG_CONSTRAINT_COST = 1000000000.0

class TransformConstraintFunction(private val point: RealVector,
                                  private val function: Function,
                                  private val t: Double,
                                  private val equalityConstraints: List<ImplicitConstraint>,
                                  private val inequalityConstraints: List<ImplicitConstraint>
) : Function {

    private var numberOfFunctionEvaluations = 0

    override fun getNumberOfFunctionEvaluations() = numberOfFunctionEvaluations

    override fun getMinimum() = 0

    override fun getStartingPoint() = point

    override fun numberOfVariables() = point.dimension

    override fun valueAt(point: RealVector, countEvaluation: Boolean): Double {
        if (countEvaluation) numberOfFunctionEvaluations++

        var totalError = function.valueAt(point)

        for (inequalityConstraint in inequalityConstraints) {
            if (!inequalityConstraint.isSatisfied(point)) {
                return WRONG_CONSTRAINT_COST
            } else {
                totalError -= (ln(inequalityConstraint.getValue(point)) * 1 / t)
            }
        }

        totalError += equalityConstraints.asSequence().map { it.getValue(point) * it.getValue(point) }.sum() * t

        return totalError
    }
}