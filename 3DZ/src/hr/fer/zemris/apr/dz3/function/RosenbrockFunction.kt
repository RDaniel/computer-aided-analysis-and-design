package hr.fer.zemris.apr.dz3.function

import hr.fer.zemris.apr.dz3.getPointValues
import org.apache.commons.math3.linear.ArrayRealVector
import org.apache.commons.math3.linear.MatrixUtils
import org.apache.commons.math3.linear.RealMatrix
import org.apache.commons.math3.linear.RealVector

class RosenbrockFunction : HessianFunction {

    private var numberOfFunctionEvaluations = 0

    private var numberOfGradientEvaluations = 0

    private var numberOfHessianEvaluations = 0

    override fun getNumberOfFunctionEvaluations() = numberOfFunctionEvaluations

    override fun getNumberOfGradientEvaluations() = numberOfGradientEvaluations

    override fun getNumberOfHessianEvaluations() = numberOfHessianEvaluations

    override fun getMinimum() = 0

    override fun getStartingPoint() = ArrayRealVector(doubleArrayOf(-1.9, 2.0))

    override fun numberOfVariables() = 2

    override fun valueAt(point: RealVector, countEvaluation: Boolean): Double {
        val values = point.getPointValues(numberOfVariables())
        if (countEvaluation) numberOfFunctionEvaluations++

        return 100 * Math.pow(values[1] - values[0] * values[0], 2.0) + (1 - values[0]) * (1 - values[0])
    }

    override fun gradientValueAt(point: RealVector): RealVector {
        val values = point.getPointValues(numberOfVariables())
        numberOfGradientEvaluations++

        val dx1 = 200 * (values[1] - values[0] * values[0]) * (-2 * values[0]) - 2 * (1 - values[0])
        val dx2 = 200 * (values[1] - values[0] * values[0])

        return ArrayRealVector(doubleArrayOf(dx1, dx2))
    }

    override fun getHessianMatrix(point: RealVector): RealMatrix {
        val values = point.getPointValues(numberOfVariables())
        numberOfHessianEvaluations++

        val dx11 = -400 * values[1] + 1200 * values[0] * values[0]
        val dx22 = 200.0
        val dx12 = -400 * values[1]

        return MatrixUtils.createRealMatrix(arrayOf(doubleArrayOf(dx11, dx12), doubleArrayOf(dx12, dx22)))
    }
}