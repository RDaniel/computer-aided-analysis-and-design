package hr.fer.zemris.apr.dz3.function

import org.apache.commons.math3.linear.RealVector

interface Function {

    fun getNumberOfFunctionEvaluations(): Int

    fun getMinimum(): Int

    fun getStartingPoint(): RealVector

    fun numberOfVariables(): Int

    fun valueAt(point: RealVector, countEvaluation: Boolean = true): Double

}