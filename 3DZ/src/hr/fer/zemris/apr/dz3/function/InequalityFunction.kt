package hr.fer.zemris.apr.dz3.function

import hr.fer.zemris.apr.dz3.constraint.ImplicitConstraint
import org.apache.commons.math3.linear.RealVector

class InequalityFunction(private val point: RealVector,
                         private val t: Double,
                         private val constraints: List<ImplicitConstraint>
) : Function {

    private var numberOfFunctionEvaluations = 0

    override fun getNumberOfFunctionEvaluations() = numberOfFunctionEvaluations

    override fun getMinimum() = 0

    override fun getStartingPoint() = point

    override fun numberOfVariables() = point.dimension

    override fun valueAt(point: RealVector, countEvaluation: Boolean): Double {
        if (countEvaluation) numberOfFunctionEvaluations++
        return -constraints.asSequence().filter { !it.isSatisfied(point) }.map { it.getValue(point) * t }.sum()
    }
}