package hr.fer.zemris.apr.dz3

import hr.fer.zemris.apr.dz3.constraint.ExplicitConstraint
import hr.fer.zemris.apr.dz3.constraint.ImplicitDifferenceConstraint
import hr.fer.zemris.apr.dz3.constraint.ImplicitSubtractionConstraint
import hr.fer.zemris.apr.dz3.function.RosenbrockFunction
import hr.fer.zemris.apr.dz3.optimization.boxAlgorithmWithConstraints
import org.apache.commons.math3.linear.ArrayRealVector

fun main(args: Array<String>) {
    val function = RosenbrockFunction()

    val explicitConstraint = ExplicitConstraint(ArrayRealVector(2, -100.0), ArrayRealVector(2, 100.0), true, true)
    val firstImplicit = ImplicitDifferenceConstraint()
    val secondImplicit = ImplicitSubtractionConstraint()
    println("Box algorithm with constraints")
    println(boxAlgorithmWithConstraints(function.getStartingPoint(), function, explicitConstraint, listOf(firstImplicit, secondImplicit)))
}