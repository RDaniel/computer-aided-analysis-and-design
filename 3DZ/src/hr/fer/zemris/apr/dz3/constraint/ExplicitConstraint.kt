package hr.fer.zemris.apr.dz3.constraint

import org.apache.commons.math3.linear.ArrayRealVector
import org.apache.commons.math3.linear.RealVector

class ExplicitConstraint(val lowerBound: RealVector = ArrayRealVector(doubleArrayOf(0.0)),
                         val upperBound: RealVector = ArrayRealVector(doubleArrayOf(0.0)),
                         private val lowerConstraint: Boolean = false,
                         private val upperConstraint: Boolean = false
) : Constraint {

    override fun isSatisfied(point: RealVector): Boolean {
        if (lowerConstraint) {
            for (i in 0 until lowerBound.dimension) {
                if (lowerBound.getEntry(i) > point.getEntry(i)) {
                    return false
                }
            }
        }

        if (upperConstraint) {
            for (i in 0 until upperBound.dimension) {
                if (upperBound.getEntry(i) < point.getEntry(i)) {
                    return false
                }
            }
        }

        return true
    }
}