package hr.fer.zemris.apr.dz3.constraint

import org.apache.commons.math3.linear.RealVector

class ImplicitMultyConstraint : ImplicitConstraint {

    override fun isSatisfied(point: RealVector) = 3 + 1.5 * point.getEntry(0) - point.getEntry(1) >= 0

    override fun getValue(point: RealVector) = 3 + 1.5 * point.getEntry(0) - point.getEntry(1)
}