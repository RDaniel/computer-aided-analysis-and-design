package hr.fer.zemris.apr.dz3.constraint

import org.apache.commons.math3.linear.RealVector

class ImplicitEqualConstraint : ImplicitConstraint {

    override fun isSatisfied(point: RealVector) = point.getEntry(1) - 1 == 0.0

    override fun getValue(point: RealVector) = point.getEntry(1) - 1
}