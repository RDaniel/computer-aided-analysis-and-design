package hr.fer.zemris.apr.dz3.constraint

import org.apache.commons.math3.linear.RealVector

interface Constraint {

    fun isSatisfied(point: RealVector): Boolean

}