package hr.fer.zemris.apr.dz3.constraint

import org.apache.commons.math3.linear.RealVector

interface ImplicitConstraint : Constraint {

    fun getValue(point: RealVector): Double

}