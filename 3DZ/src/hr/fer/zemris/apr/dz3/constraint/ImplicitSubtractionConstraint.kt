package hr.fer.zemris.apr.dz3.constraint

import org.apache.commons.math3.linear.RealVector

class ImplicitSubtractionConstraint : ImplicitConstraint {

    override fun isSatisfied(point: RealVector) = 2 - point.getEntry(0) >= 0

    override fun getValue(point: RealVector) = 2 - point.getEntry(0)
}