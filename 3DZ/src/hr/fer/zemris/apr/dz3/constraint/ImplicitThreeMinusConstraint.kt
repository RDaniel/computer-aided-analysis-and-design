package hr.fer.zemris.apr.dz3.constraint

import org.apache.commons.math3.linear.RealVector

class ImplicitThreeMinusConstraint : ImplicitConstraint {

    override fun isSatisfied(point: RealVector) = 3 - point.getEntry(0) - point.getEntry(1) >= 0

    override fun getValue(point: RealVector) = 3 - point.getEntry(0) - point.getEntry(1)
}