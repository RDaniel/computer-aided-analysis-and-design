package hr.fer.zemris.apr.dz3

import org.apache.commons.math3.linear.ArrayRealVector
import org.apache.commons.math3.linear.RealVector
import java.util.*
import kotlin.math.absoluteValue

const val EPSILON = 1E-12

fun Double.isZero() = this.absoluteValue < EPSILON

fun RealVector.getPointValues(dimension: Int): DoubleArray {
    if (this.dimension != dimension) {
        throw IllegalArgumentException("Wrong point dimensions!")
    }

    return toArray()
}

fun Array<Double>.string(): String {
    return Arrays.toString(this)
}

fun Double.asVector(): RealVector {
    return ArrayRealVector(doubleArrayOf(this))
}