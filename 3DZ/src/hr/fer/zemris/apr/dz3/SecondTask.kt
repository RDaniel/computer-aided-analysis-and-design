package hr.fer.zemris.apr.dz3

import hr.fer.zemris.apr.dz3.function.SecondFunction
import hr.fer.zemris.apr.dz3.optimization.gradientDescentAlgorithm

fun main(args: Array<String>) {
    var function = SecondFunction()
//    val function = RosenbrockFunction()

    println("Gradient descent method solution = ${gradientDescentAlgorithm(function, function.getStartingPoint())}")
    println("Function evaluations -> ${function.getNumberOfFunctionEvaluations()}")
    println("Gradient calculations -> ${function.getNumberOfGradientEvaluations()}")

//    println("Newton raphson method solution = ${newtonRaphsonAlgorithm(function, function.getStartingPoint())}")
//    println("Function evaluations -> ${function.getNumberOfGradientEvaluations()}")
//    println("Gradient calculations -> ${function.getNumberOfGradientEvaluations()}")
//    println("Hessian calculations -> ${function.getNumberOfHessianEvaluations()}")
}