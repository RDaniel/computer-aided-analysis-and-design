package hr.fer.zemris.apr.dz3

import hr.fer.zemris.apr.dz3.constraint.ImplicitDifferenceConstraint
import hr.fer.zemris.apr.dz3.constraint.ImplicitSubtractionConstraint
import hr.fer.zemris.apr.dz3.function.RosenbrockFunction
import hr.fer.zemris.apr.dz3.optimization.transformConstraintAlgorithm

fun main(args: Array<String>) {
    val function = RosenbrockFunction()

    val firstImplicit = ImplicitDifferenceConstraint()
    val secondImplicit = ImplicitSubtractionConstraint()
    println(transformConstraintAlgorithm(function.getStartingPoint(), function, 1.0, emptyList(), listOf(firstImplicit, secondImplicit)))
}