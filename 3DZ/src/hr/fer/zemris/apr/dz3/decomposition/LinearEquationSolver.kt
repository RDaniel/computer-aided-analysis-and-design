package hr.fer.zemris.apr.dz3.decomposition

import hr.fer.zemris.apr.dz3.isZero
import org.apache.commons.math3.linear.MatrixUtils
import org.apache.commons.math3.linear.RealMatrix
import org.apache.commons.math3.linear.RealVector
import kotlin.math.absoluteValue

fun solveEquationUsingLUP(systemMatrix: RealMatrix, solutionVector: RealVector): RealVector {
    lupDecomposition(systemMatrix, solutionVector)

    val y = forwardSubstitution(systemMatrix, solutionVector)

    return backwardSubstitution(systemMatrix, y)
}

fun forwardSubstitution(systemMatrix: RealMatrix, solutionVector: RealVector): RealVector {
    for (i in 0 until systemMatrix.rowDimension - 1) {
        for (j in i + 1 until systemMatrix.rowDimension) {
            solutionVector.setEntry(j, solutionVector.getEntry(j) - systemMatrix.getEntry(j, i) * solutionVector.getEntry(i))
        }
    }

    return solutionVector
}

fun backwardSubstitution(systemMatrix: RealMatrix, vector: RealVector): RealVector {
    for (i in (systemMatrix.rowDimension - 1) downTo 0) {
        checkIfZero(systemMatrix.getEntry(i, i))
        vector.setEntry(i, vector.getEntry(i) / systemMatrix.getEntry(i, i))
        for (j in 0 until i) {
            vector.setEntry(j, vector.getEntry(j) - systemMatrix.getEntry(j, i) * vector.getEntry(i))
        }
    }

    return vector
}

fun lupDecomposition(systemMatrix: RealMatrix, solutionVector: RealVector? = null): RealMatrix {
    for (i in 0 until systemMatrix.rowDimension - 1) {
        val pivotIndex = choosePivotElement(systemMatrix, i)
        if (pivotIndex != i) {
            swapRows(i, pivotIndex, systemMatrix)
            if (solutionVector != null) {
                swapSolutionVector(solutionVector, i, pivotIndex)
            }
        }

        for (j in i + 1 until systemMatrix.rowDimension) {
            checkIfZero(systemMatrix.getEntry(i, i))
            systemMatrix.setEntry(j, i, systemMatrix.getEntry(j, i) / systemMatrix.getEntry(i, i))
            for (k in i + 1 until systemMatrix.rowDimension) {
                systemMatrix.setEntry(j, k, systemMatrix.getEntry(j, k) - systemMatrix.getEntry(j, i) * systemMatrix.getEntry(i, k))
            }
        }
    }

    return systemMatrix
}

private fun swapSolutionVector(solutionVector: RealVector, from: Int, to: Int) {
    val columnRealMatrix = MatrixUtils.createColumnRealMatrix(solutionVector.toArray())
    swapRows(from, to, columnRealMatrix)
    for (z in 0 until columnRealMatrix.getColumn(0).size) {
        solutionVector.setEntry(z, columnRealMatrix.getColumnVector(0).getEntry(z))
    }
}

private fun swapRows(from: Int, to: Int, matrix: RealMatrix) {
    for (i in 0 until matrix.columnDimension) {
        val temp = matrix.getEntry(from, i)
        matrix.setEntry(from, i, matrix.getEntry(to, i))
        matrix.setEntry(to, i, temp)
    }
}

private fun choosePivotElement(system: RealMatrix, currentIndex: Int): Int {
    var max = system.getEntry(currentIndex, currentIndex).absoluteValue
    var pivot = currentIndex

    for (i in currentIndex + 1 until system.columnDimension) {
        if (max < system.getEntry(i, currentIndex).absoluteValue) {
            pivot = i
            max = system.getEntry(i, currentIndex)
        }
    }

    return pivot
}

private fun checkIfZero(scalar: Double) {
    if (scalar.isZero()) {
        throw IllegalArgumentException("Cannot divide with zero.")
    }
}