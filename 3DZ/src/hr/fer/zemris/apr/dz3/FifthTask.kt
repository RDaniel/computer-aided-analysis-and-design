package hr.fer.zemris.apr.dz3

import hr.fer.zemris.apr.dz3.constraint.ImplicitEqualConstraint
import hr.fer.zemris.apr.dz3.constraint.ImplicitMultyConstraint
import hr.fer.zemris.apr.dz3.constraint.ImplicitThreeMinusConstraint
import hr.fer.zemris.apr.dz3.function.FourthFunction
import hr.fer.zemris.apr.dz3.optimization.transformConstraintAlgorithm
import org.apache.commons.math3.linear.ArrayRealVector

fun main(args: Array<String>) {
    val function = FourthFunction()

    val firstImplicit = ImplicitThreeMinusConstraint()
    val secondImplicit = ImplicitMultyConstraint()
    val thirdImplicit = ImplicitEqualConstraint()
    println(transformConstraintAlgorithm(ArrayRealVector(doubleArrayOf(5.0, 5.0)), function, 1.0, listOf(thirdImplicit), listOf(firstImplicit, secondImplicit)))

}