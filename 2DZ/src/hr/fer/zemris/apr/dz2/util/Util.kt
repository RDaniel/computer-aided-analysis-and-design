package hr.fer.zemris.apr.dz2.util

import java.nio.file.Files
import java.nio.file.Path

fun getProblemDefinition(path: Path): List<Double> {
    val definition = String(Files.readAllBytes(path))

    val values = mutableListOf<Double>()
    definition.split("\\s+".toRegex()).forEach { values.add(it.toDouble()) }

    return values
}

fun multiply(simplexPoint: Array<Double>, constant: Double): Array<Double> {
    val dimension = simplexPoint.size

    val result = Array(dimension) { _ -> 0.0 }

    for (i in 0 until dimension) {
        result[i] = simplexPoint[i] * constant
    }

    return result
}

fun add(first: Array<Double>, second: Array<Double>): Array<Double> {
    val dimension = first.size

    val result = Array(dimension) { _ -> 0.0 }

    for (i in 0 until dimension) {
        result[i] = first[i] + second[i]
    }

    return result
}