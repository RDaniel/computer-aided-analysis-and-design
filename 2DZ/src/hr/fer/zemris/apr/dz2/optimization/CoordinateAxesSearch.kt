package hr.fer.zemris.apr.dz2.optimization

import hr.fer.zemris.apr.dz2.function.AbstractFunction
import hr.fer.zemris.apr.dz2.function.LambdaWrapperFunction
import hr.fer.zemris.apr.dz2.util.getProblemDefinition
import java.nio.file.Path

private const val STARTING_POINT = 0.0

fun coordinateAxesSearch(problemPath: Path, function: AbstractFunction): Array<Double> {
    val problemDefinition = getProblemDefinition(problemPath)
    return coordinateAxesSearch(problemDefinition.drop(1).toTypedArray(), function, problemDefinition[0])
}

fun coordinateAxesSearch(x: Array<Double>, function: AbstractFunction, e: Double = 10E-6): Array<Double> {
    val n = x.size

    do {
        val xs = x.copyOf()
        for (i in 0 until n) {
            val wrapper = LambdaWrapperFunction(x, function, i)
            val lambdaValue = goldenCutAlgorithm(STARTING_POINT, wrapper)
            x[i] += lambdaValue
        }
    } while (shouldContinue(xs, x, e))

    return x
}

private fun shouldContinue(previous: Array<Double>, current: Array<Double>, e: Double): Boolean {
    val n = previous.size

    for (i in 0 until n) {
        if (Math.abs(previous[i] - current[i]) <= e) return false
    }

    return true
}
