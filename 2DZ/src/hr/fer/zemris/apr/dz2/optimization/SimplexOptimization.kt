package hr.fer.zemris.apr.dz2.optimization

import hr.fer.zemris.apr.dz2.function.AbstractFunction
import hr.fer.zemris.apr.dz2.string
import hr.fer.zemris.apr.dz2.util.add
import hr.fer.zemris.apr.dz2.util.getProblemDefinition
import hr.fer.zemris.apr.dz2.util.multiply
import java.nio.file.Path

fun simplexOptimization(definitionFile: Path, function: AbstractFunction): Array<Double> {
    val problemDefinition = getProblemDefinition(definitionFile)
    val shift = problemDefinition[0]
    val alpha = problemDefinition[1]
    val beta = problemDefinition[2]
    val gamma = problemDefinition[3]
    val sigma = problemDefinition[4]
    val epsilon = problemDefinition[5]
    val startingX = problemDefinition.drop(6).toTypedArray()

    return simplexOptimization(startingX, function, shift, alpha, beta, gamma, sigma, epsilon)
}

fun simplexOptimization(startingX: Array<Double>, function: AbstractFunction, shift: Double = 1.0, alpha: Double = 1.0, beta: Double = 0.5, gamma: Double = 1.0, sigma: Double = 0.5, epsilon: Double = 10E-6): Array<Double> {
    val n: Int = startingX.size
    val simplexPoints = mutableListOf(startingX)

    for (i in 0 until n) {
        val values = startingX.copyOf()
        values[i] += shift
        simplexPoints.add(values)
    }

    var bestPoint: Int

    do {
        val functionValues = simplexPoints.mapIndexed { index, doubles -> function.evaluate(doubles) to index }.toMap().toSortedMap()
        bestPoint = functionValues[functionValues.firstKey()]!!
        val worstPoint = functionValues[functionValues.lastKey()]!!

        val centroid = calculateCentroid(simplexPoints, worstPoint, n)

        val xr = reflection(centroid, simplexPoints[worstPoint], alpha)
        val reflectionValue = function.evaluate(xr)
        if (reflectionValue < functionValues.firstKey()) {
            val xe = expansion(centroid, simplexPoints[bestPoint], gamma)

            if (function.evaluate(xe) < functionValues.firstKey()) {
                simplexPoints[worstPoint] = xe
            } else {
                simplexPoints[worstPoint] = xr
            }
        } else {
            if (isReflectionBigger(reflectionValue, functionValues, worstPoint)) {
                if (reflectionValue < functionValues.lastKey()) {
                    simplexPoints[worstPoint] = xr
                }

                val xk = contraction(centroid, simplexPoints[worstPoint], beta)
                if (function.evaluate(xk) < functionValues.lastKey()) {
                    simplexPoints[worstPoint] = xk
                } else {
                    moveAllPointsToBestPoint(simplexPoints, bestPoint, sigma)
                }
            } else {
                simplexPoints[worstPoint] = xr
            }
        }

        println("Centroid=${centroid.string()}")
        println("f(centroid)=${function.evaluate(centroid, false)}")
    } while (checkConstraint(simplexPoints, centroid, function, epsilon))

    return simplexPoints[bestPoint]
}

fun checkConstraint(simplexPoints: List<Array<Double>>, centroid: Array<Double>, function: AbstractFunction, epsilon: Double): Boolean {
    var totalSum = 0.0
    for (simplexPoint in simplexPoints) {
        totalSum += Math.pow((function.evaluate(simplexPoint) - function.evaluate(centroid)), 2.0)
    }

    totalSum /= simplexPoints.size

    return Math.sqrt(totalSum) > epsilon
}

fun moveAllPointsToBestPoint(simplexPoints: MutableList<Array<Double>>, bestPoint: Int, sigma: Double) {
    val size = simplexPoints.size

    for (i in 0 until size) {
        if (i == bestPoint) continue
        simplexPoints[i] = multiply(add(simplexPoints[i], simplexPoints[bestPoint]), sigma)
    }
}

fun isReflectionBigger(evaluate: Double, functionValues: Map<Double, Int>, worstPoint: Int): Boolean {
    for (functionValue in functionValues) {
        if (functionValue.value == worstPoint) continue
        if (evaluate < functionValue.key) return false
    }

    return true
}

fun reflection(centroid: Array<Double>, worstPoint: Array<Double>, alpha: Double) =
    add(multiply(centroid, alpha + 1), multiply(worstPoint, -alpha))

fun contraction(centroid: Array<Double>, worstPoint: Array<Double>, beta: Double) =
    add(multiply(centroid, 1 - beta), multiply(worstPoint, beta))

fun expansion(centroid: Array<Double>, reflection: Array<Double>, gamma: Double) =
    add(multiply(centroid, 1 - gamma), multiply(reflection, gamma))

fun calculateCentroid(simplexPoints: List<Array<Double>>, worstPoint: Int, dimension: Int): Array<Double> {
    val size = simplexPoints.size
    var centroid = Array(dimension) { _ -> 0.0 }

    for (i in 0 until size) {
        if (i == worstPoint) continue
        centroid = add(centroid, simplexPoints[i])
    }

    return multiply(centroid, 1.0 / (size - 1))
}