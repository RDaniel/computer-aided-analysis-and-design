package hr.fer.zemris.apr.dz2.optimization

import hr.fer.zemris.apr.dz2.function.AbstractFunction
import hr.fer.zemris.apr.dz2.string
import hr.fer.zemris.apr.dz2.util.add
import hr.fer.zemris.apr.dz2.util.getProblemDefinition
import hr.fer.zemris.apr.dz2.util.multiply
import java.nio.file.Path

fun hookeJeevesAlgorithm(definitionPath: Path, function: AbstractFunction): Array<Double> {
    val problemDefinition = getProblemDefinition(definitionPath)
    return hookeJeevesAlgorithm(problemDefinition.drop(2).toTypedArray(), function, problemDefinition[0], problemDefinition[1])
}

fun hookeJeevesAlgorithm(x0: Array<Double>, function: AbstractFunction, shift: Double = 3.0, epsilon: Double = 10E-6): Array<Double> {
    var xp = x0.copyOf()
    var xb = x0.copyOf()
    var dx = shift
    do {
        val xn = explore(xp, dx, function)
        if (function.evaluate(xn) < function.evaluate(xb)) {
            xp = add(multiply(xn, 2.0), multiply(xb, -1.0))
            xb = xn
        } else {
            dx /= 2.0
            xp = xb.copyOf()
        }

        println(dx)
        printPoints(xb, xp, xn, function)
    } while (dx > epsilon)

    return xb
}

private fun explore(xp: Array<Double>, shift: Double, function: AbstractFunction): Array<Double> {
    val x = xp.copyOf()
    val size = xp.size

    for (i in 0 until size) {
        val pValue = function.evaluate(x)
        x[i] += shift
        var nValue = function.evaluate(x)

        if (nValue > pValue) {
            x[i] -= 2 * shift
            nValue = function.evaluate(x)

            if (nValue > pValue) {
                x[i] += shift
            }
        }
    }

    return x
}

private fun printPoints(xb: Array<Double>, xp: Array<Double>, xn: Array<Double>, function: AbstractFunction) {
    println("xb=${xb.string()} xp=${xp.string()} xn=${xn.string()}")
    println("f(xb)=${function.evaluate(xb, false)} f(xp)=${function.evaluate(xp, false)} f(xn)=${function.evaluate(xn, false)}")
}
