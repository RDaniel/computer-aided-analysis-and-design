package hr.fer.zemris.apr.dz2.optimization

import hr.fer.zemris.apr.dz2.function.AbstractFunction
import hr.fer.zemris.apr.dz2.util.getProblemDefinition
import java.nio.file.Path

fun goldenCutAlgorithm(definitionFile: Path, function: AbstractFunction): Double {
    val values = getProblemDefinition(definitionFile)
    return if (values.size == 2) {
        goldenCutAlgorithm(values[0], function, values[1])
    } else {
        goldenCutAlgorithm(values[0], values[1], function, values[2])
    }
}

fun goldenCutAlgorithm(startingPoint: Double, function: AbstractFunction, e: Double = 10E-6): Double {
    val interval = unimodalIntervalFinder(1.0, startingPoint, function)
    return goldenCutAlgorithm(interval[0], interval[1], function, e)
}

fun goldenCutAlgorithm(left: Double, right: Double, function: AbstractFunction, e: Double = 10E-6): Double {
    val k = 0.5 * (Math.sqrt(5.0) - 1)

    var a = left
    var b = right
    var c = b - k * (b - a)
    var d = a + k * (b - a)

    var fa = function.evaluate(a)
    var fb = function.evaluate(b)
    var fc = function.evaluate(c)
    var fd = function.evaluate(d)

    var step = 0

    printValues(step, a, b, c, d, fa, fb, fc, fd)

    step = 1

    while ((b - a) > e) {
        if (fc < fd) {
            b = d
            d = c
            c = b - k * (b - a)
            fb = fd
            fd = fc
            fc = function.evaluate(c)
        } else {
            a = c
            c = d
            d = a + k * (b - a)
            fa = fc
            fc = fd
            fd = function.evaluate(d)
        }

        printValues(step, a, b, c, d, fa, fb, fc, fd)
        step++
    }

    return (a + b) / 2.0
}

fun unimodalIntervalFinder(shift: Double, x: Double, function: AbstractFunction): Array<Double> {
    var left = x - shift
    var right = x + shift
    var m = x
    var step = 1

    var fm = function.evaluate(x)
    var fl = function.evaluate(left)
    var fr = function.evaluate(right)

    if (fm < fr && fm < fl)
        return arrayOf(left, right)
    else if (fm > fr) {
        do {
            left = m
            m = right
            fm = fr
            step *= 2
            right = x + shift * step
            fr = function.evaluate(right)
        } while (fm > fr)
    } else {
        do {
            right = m
            m = left
            fm = fl
            step *= 2
            left = x - shift * step
            fl = function.evaluate(left)
        } while (fm > fl)
    }

    return arrayOf(left, right)
}

private fun printValues(step: Int, a: Double, b: Double, c: Double, d: Double, fa: Double, fb: Double, fc: Double, fd: Double) {
    println("Iteration = $step")
    println("a=$a b=$b c=$c d=$d")
    println("f(a)=$fa f(b)=$fb f(c)=$fc f(d)=$fd")
}