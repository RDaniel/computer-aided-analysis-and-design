package hr.fer.zemris.apr.dz2

import java.util.*

fun Array<Double>.string(): String {
    return Arrays.toString(this)
}