package hr.fer.zemris.apr.dz2

import hr.fer.zemris.apr.dz2.function.JakobovichFunction
import hr.fer.zemris.apr.dz2.function.RosenbrockFunction
import hr.fer.zemris.apr.dz2.function.SecondFunction
import hr.fer.zemris.apr.dz2.function.ThirdFunction
import hr.fer.zemris.apr.dz2.optimization.coordinateAxesSearch
import hr.fer.zemris.apr.dz2.optimization.hookeJeevesAlgorithm
import hr.fer.zemris.apr.dz2.optimization.simplexOptimization

fun main(args: Array<String>) {
    val functions = listOf(RosenbrockFunction(), SecondFunction(), ThirdFunction(5), JakobovichFunction())

    val resultBuilder = StringBuilder()

    for (function in functions) {
        resultBuilder.append("${function.getName()} --> ")
        resultBuilder.append("Algorithm = Coordinate Axes > ")
        resultBuilder.append("Minimum = ${coordinateAxesSearch(function.startingPoint, function).string()} | ")
        resultBuilder.append("Invocations = ${function.numberOfInvocations} ")
        function.reset()

        resultBuilder.append("Algorithm = Simplex > ")
        resultBuilder.append("Minimum = ${simplexOptimization(function.startingPoint, function).string()} | ")
        resultBuilder.append("Invocations = ${function.numberOfInvocations} ")
        function.reset()

        resultBuilder.append("Algorithm = Hooke Jeeves > ")
        resultBuilder.append("Minimum = ${hookeJeevesAlgorithm(function.startingPoint, function).string()} | ")
        resultBuilder.appendln("Invocations = ${function.numberOfInvocations} ")
        function.reset()
    }

    println()
    println(resultBuilder.toString())
}