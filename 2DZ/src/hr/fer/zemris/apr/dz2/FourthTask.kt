package hr.fer.zemris.apr.dz2

import hr.fer.zemris.apr.dz2.function.RosenbrockFunction
import hr.fer.zemris.apr.dz2.optimization.simplexOptimization

fun main(args: Array<String>) {
    val function = RosenbrockFunction()

    val resultBuilder = StringBuilder()
    resultBuilder.append("Algorithm = Simplex > ${System.lineSeparator()}")

    val startingPoints = listOf(arrayOf(0.5, 0.5), arrayOf(20.0, 20.0))
    for (startingPoint in startingPoints) {
        resultBuilder.append("Starting point = ${startingPoint.string()} ${System.lineSeparator()}")
        for (shift in 1..20 step 2) {
            resultBuilder.append("Shift = $shift ")
            resultBuilder.append("Minimum = ${simplexOptimization(startingPoint, function, shift = shift.toDouble()).string()} | ")
            resultBuilder.appendln("Invocations = ${function.numberOfInvocations} ")
            function.reset()
        }
    }

    println()
    println(resultBuilder.toString())
}