package hr.fer.zemris.apr.dz2

import hr.fer.zemris.apr.dz2.function.SchaffersFunction
import hr.fer.zemris.apr.dz2.optimization.simplexOptimization
import java.util.*

fun main(args: Array<String>) {
    val function = SchaffersFunction(2)

    val resultBuilder = StringBuilder()
    resultBuilder.append("Algorithm = Simplex > ${System.lineSeparator()}")

    val random = Random()

    var zeroCounter = 0
    val numberOfTries = 10000

    for (i in 0..numberOfTries) {
        val startingPoint = arrayOf(-50 + random.nextDouble() * 100, -50 + random.nextDouble() * 100)
        resultBuilder.append("Starting point = ${startingPoint.string()} ${System.lineSeparator()}")

        val functionValue = function.evaluate(simplexOptimization(startingPoint, function))
        if (functionValue < 10E-4) zeroCounter++
        resultBuilder.appendln("f(minimum) = $functionValue | ")

        function.reset()
    }

    resultBuilder.append("Number of tries = $numberOfTries Found minimum = $zeroCounter")
    println(resultBuilder.toString())
}