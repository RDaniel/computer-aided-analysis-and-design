package hr.fer.zemris.apr.dz2

import hr.fer.zemris.apr.dz2.function.JakobovichFunction
import hr.fer.zemris.apr.dz2.optimization.hookeJeevesAlgorithm
import hr.fer.zemris.apr.dz2.optimization.simplexOptimization

fun main(args: Array<String>) {
    val function = JakobovichFunction()

    val resultBuilder = StringBuilder()

    resultBuilder.append("Algorithm = Simplex > ")
    resultBuilder.append("Minimum = ${simplexOptimization(arrayOf(5.0, 5.0), function).string()} | ")
    resultBuilder.append("Invocations = ${function.numberOfInvocations} ")
    function.reset()

    resultBuilder.append("Algorithm = Hooke Jeeves > ")
    resultBuilder.append("Minimum = ${hookeJeevesAlgorithm(arrayOf(5.0, 5.0), function).string()} | ")
    resultBuilder.appendln("Invocations = ${function.numberOfInvocations} ")
    function.reset()

    println()
    println(resultBuilder.toString())
}