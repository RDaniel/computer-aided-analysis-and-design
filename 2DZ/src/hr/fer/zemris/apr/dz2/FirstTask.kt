package hr.fer.zemris.apr.dz2

import hr.fer.zemris.apr.dz2.function.MinThreeFunction
import hr.fer.zemris.apr.dz2.optimization.coordinateAxesSearch
import hr.fer.zemris.apr.dz2.optimization.goldenCutAlgorithm
import hr.fer.zemris.apr.dz2.optimization.hookeJeevesAlgorithm
import hr.fer.zemris.apr.dz2.optimization.simplexOptimization
import java.nio.file.Paths

fun main(args: Array<String>) {
    val function = MinThreeFunction()

    val resultBuilder = StringBuilder()

    val startingPoint = 10.0

    resultBuilder.appendln("Starting point = $startingPoint")

    resultBuilder.append("Algorithm = Golden cut > ")
    resultBuilder.append("Minimum = ${goldenCutAlgorithm(Paths.get("goldenCutProblemDefinition"), function)} | ")
    resultBuilder.appendln("Number of function invocations = ${function.numberOfInvocations} | ")
    function.reset()

    resultBuilder.append("Algorithm = Coordinate Axes > ")
    resultBuilder.append("Minimum = ${coordinateAxesSearch(arrayOf(startingPoint), function).string()} | ")
    resultBuilder.appendln("Invocations = ${function.numberOfInvocations} ")
    function.reset()

    resultBuilder.append("Algorithm = Simplex > ")
    resultBuilder.append("Minimum = ${simplexOptimization(arrayOf(startingPoint), function).string()} | ")
    resultBuilder.appendln("Invocations = ${function.numberOfInvocations} ")
    function.reset()

    resultBuilder.append("Algorithm = Hooke Jeeves > ")
    resultBuilder.append("Minimum = ${hookeJeevesAlgorithm(arrayOf(startingPoint), function).string()} | ")
    resultBuilder.appendln("Invocations = ${function.numberOfInvocations} ")
    function.reset()


    println()
    println(resultBuilder.toString())
}