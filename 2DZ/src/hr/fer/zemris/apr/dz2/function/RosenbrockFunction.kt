package hr.fer.zemris.apr.dz2.function

class RosenbrockFunction : AbstractFunction() {

    override var startingPoint = arrayOf(-1.9, 2.0)

    override val minimum: Double = 0.0

    override fun calculate(x: Array<Double>) =
        100 * (x[1] - x[0] * x[0]) * (x[1] - x[0] * x[0]) + (1 - x[0]) * (1 - x[0])

    override fun getName() = "Rosenbrock function"
}