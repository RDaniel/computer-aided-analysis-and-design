package hr.fer.zemris.apr.dz2.function

class MinThreeFunction : AbstractFunction() {

    override var startingPoint = arrayOf(0.0)

    override val minimum: Double = 0.0

    override fun calculate(x: Array<Double>) = (x[0] - 3) * (x[0] - 3)

    override fun getName() = "Min three function"
}