package hr.fer.zemris.apr.dz2.function

class SchaffersFunction(n: Int) : AbstractFunction() {

    override var startingPoint = Array(n) { _ -> 5.0 }

    override val minimum = 0.0

    override fun calculate(x: Array<Double>): Double {
        val squaredSum = x.map { it * it }.sum()
        return 0.5 + (Math.pow(Math.sin(Math.sqrt(squaredSum)), 2.0) - 0.5) / Math.pow((1 + 0.001 * squaredSum), 2.0)
    }

    override fun getName() = "Schaffers function"
}