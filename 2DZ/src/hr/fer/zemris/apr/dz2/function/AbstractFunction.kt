package hr.fer.zemris.apr.dz2.function

abstract class AbstractFunction {

    var numberOfInvocations: Int = 0

    abstract var startingPoint: Array<Double>

    abstract val minimum: Double

    fun reset() {
        numberOfInvocations = 0
    }

    fun evaluate(x: Array<Double>, count: Boolean = true): Double {
        if (count) {
            numberOfInvocations++
        }

        return calculate(x)
    }

    fun evaluate(x: Double, count: Boolean = true): Double {
        return evaluate(doubleArrayOf(x).toTypedArray(), count)
    }

    abstract fun getName(): String

    protected abstract fun calculate(x: Array<Double>): Double
}