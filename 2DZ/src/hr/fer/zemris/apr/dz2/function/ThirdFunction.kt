package hr.fer.zemris.apr.dz2.function

class ThirdFunction(n: Int) : AbstractFunction() {

    override var startingPoint = Array(n) { _ -> 0.0 }

    override val minimum: Double = 0.0

    override fun calculate(x: Array<Double>) = x.mapIndexed { index, d -> (d - index - 1) * (d - index - 1) }.sum()

    override fun getName() = "Third function"
}