package hr.fer.zemris.apr.dz2.function

class SecondFunction : AbstractFunction() {

    override var startingPoint: Array<Double> = arrayOf(0.1, 0.3)

    override val minimum: Double = 0.0

    override fun calculate(x: Array<Double>) =
        (x[0] - 4) * (x[0] - 4) + 4 * (x[1] - 2) * (x[1] - 2)

    override fun getName() = "Second function"
}