package hr.fer.zemris.apr.dz2.function

class JakobovichFunction : AbstractFunction() {

    override var startingPoint = arrayOf(5.1, 1.1)

    override val minimum = 0.0

    override fun calculate(x: Array<Double>) =
        Math.abs((x[0] - x[1]) * (x[0] + x[1])) + Math.sqrt(x[0] * x[0] + x[1] * x[1])

    override fun getName() = "Jakobovich function"
}