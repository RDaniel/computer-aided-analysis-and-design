package hr.fer.zemris.apr.dz2.function

class LambdaWrapperFunction(
    private val values: Array<Double>,
    private val function: AbstractFunction,
    private val currentAxis: Int)
    : AbstractFunction() {

    override var startingPoint = arrayOf(0.0)
    override val minimum = function.minimum

    override fun calculate(x: Array<Double>): Double {
        val valuesCopy = values.copyOf()
        valuesCopy[currentAxis] += x[0]

        return function.evaluate(valuesCopy)
    }

    override fun getName() = "Lambda wrapper function"
}