package hr.fer.zemris.apr.dz5

import hr.fer.zemris.apr.dz5.algorithm.rungeKutta
import hr.fer.zemris.apr.dz5.algorithm.trapezoidalRule
import hr.fer.zemris.apr.dz5.data.Matrix
import hr.fer.zemris.apr.dz5.graphics.LineGraph
import java.nio.file.Files
import java.nio.file.Paths

fun printMatrixInverse(matrix: Matrix) {
    println("Matrix inverse")
    try {
        println(matrix.inverse())
    } catch (ex: Exception) {
        println(ex.message)
    }
}

fun solveSystemAndShowResults(A: Matrix, B: Matrix, startingX: Matrix, T: Double, tMax: Double, printBorder: Int = 5) {
    println("Trapezoidal rule")
    val trapStrategy = StepStrategy.getNewStrategy("resultTrapezoidal.txt", printBorder)
    trapezoidalRule(A, B, startingX, T, tMax) { t, matrix -> trapStrategy.process(t, matrix) }

    println()

    println("Runge kutta")
    val rungeStrategy = StepStrategy.getNewStrategy("resultRunge.txt", printBorder)
    rungeKutta(A, B, startingX, T, tMax) { t, matrix -> rungeStrategy.process(t, matrix) }

    var graph = LineGraph()
    graph.addCategory("runge", rungeStrategy.xs, rungeStrategy.positions)
    graph.addCategory("trapezoidal", trapStrategy.xs, trapStrategy.positions)
    graph.drawGraph("Runge vs Trapezoidal", "Position")

    graph = LineGraph()
    graph.addCategory("runge", rungeStrategy.xs, rungeStrategy.velocities)
    graph.addCategory("trapezoidal", trapStrategy.xs, trapStrategy.velocities)
    graph.drawGraph("Runge vs Trapezoidal", "Velocity")
}

fun rungeKuttaStepsComparison(A: Matrix, B: Matrix, startingX: Matrix, steps: List<Double>) {
    val graph = LineGraph()
    for (step in steps) {
        val rungeValues = mutableListOf<Double>()
        val xs = mutableListOf<Double>()
        val strategy = { t: Double, matrix: Matrix ->
            rungeValues.add(matrix[0])
            xs.add(t)
            Unit
        }

        rungeKutta(A, B, startingX, step, 4.0, strategy)
        graph.addCategory("step = $step", xs, rungeValues)
    }

    graph.drawGraph("Runge kutta steps comparison", "Position")
}

class StepStrategy(
    fileName: String,
    private val printBorder: Int,
    val xs: MutableList<Double>,
    val positions: MutableList<Double>,
    val velocities: MutableList<Double>
) {

    private val writer = Files.newBufferedWriter(Paths.get(fileName))

    var printCounter = 0

    companion object {
        fun getNewStrategy(fileName: String, printBorder: Int): StepStrategy {
            return StepStrategy(fileName, printBorder, mutableListOf(), mutableListOf(), mutableListOf())
        }
    }

    fun process(t: Double, matrix: Matrix) {
        writer.write("$t ${matrix[0]} ${matrix[1]}")
        writer.newLine()
        writer.flush()

        xs.add(t)

        printCounter++
        if (printCounter >= printBorder) {
            println("t = ${String.format("%.1f", t)} x1 = ${matrix[0]} x2 = ${matrix[1]}")
            printCounter = 0
        }

        positions.add(matrix[0])
        velocities.add(matrix[1])
    }
}