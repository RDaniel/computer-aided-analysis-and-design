package hr.fer.zemris.apr.dz5

import hr.fer.zemris.apr.dz5.data.Matrix

fun main(args: Array<String>) {
    val A = Matrix("matrix3")
    val B = Matrix("matrixB")
    val startingX = Matrix("starting3")

    solveSystemAndShowResults(A, B, startingX, 0.1, 5.0)
}