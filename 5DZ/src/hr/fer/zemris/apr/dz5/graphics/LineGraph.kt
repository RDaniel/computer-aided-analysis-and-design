package hr.fer.zemris.apr.dz5.graphics

import org.jfree.chart.ChartFactory
import org.jfree.chart.ChartPanel
import org.jfree.chart.JFreeChart
import org.jfree.chart.axis.CategoryLabelPositions
import org.jfree.chart.plot.PlotOrientation
import org.jfree.data.category.DefaultCategoryDataset
import java.awt.Dimension
import javax.swing.JFrame
import javax.swing.WindowConstants


class LineGraph : JFrame() {

    private val dataset = DefaultCategoryDataset()

    fun addCategory(categoryName: String, xs: List<Double>, ys: List<Double>): DefaultCategoryDataset {
        xs.forEachIndexed { index, value ->
            dataset.addValue(ys[index], categoryName, String.format("%.1f", value))
        }

        return dataset
    }

    fun drawGraph(graphTitle: String, yAxis: String) {
        val lineChart: JFreeChart = ChartFactory.createLineChart(
            graphTitle,
            "Time", yAxis,
            dataset,
            PlotOrientation.VERTICAL,
            true, true, false
        )

        lineChart.categoryPlot.domainAxis.categoryLabelPositions =
                CategoryLabelPositions.createUpRotationLabelPositions(Math.PI / 2.0)

        val chartPanel = ChartPanel(lineChart)
        chartPanel.preferredSize = Dimension(1000, 500)
        contentPane = chartPanel

        pack()

        defaultCloseOperation = WindowConstants.DISPOSE_ON_CLOSE
        isVisible = true
    }
}