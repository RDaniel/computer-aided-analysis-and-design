package hr.fer.zemris.apr.dz5

import hr.fer.zemris.apr.dz5.data.Matrix

fun main(args: Array<String>) {
    val systemMatrix = Matrix("matrix2")
    printMatrixInverse(systemMatrix)
}