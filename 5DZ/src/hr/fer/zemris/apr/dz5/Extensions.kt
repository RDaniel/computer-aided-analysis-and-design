package hr.fer.zemris.apr.dz5

import kotlin.math.absoluteValue

const val EPSILON = 1E-12

fun Double.isZero() = this.absoluteValue < EPSILON