package hr.fer.zemris.apr.dz5.data

import hr.fer.zemris.apr.dz5.isZero
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.*
import kotlin.math.absoluteValue


class Matrix {

    private var rows: Int = 0
    private var columns: Int = 0

    private lateinit var values: Array<DoubleArray>

    constructor(rows: Int, columns: Int) {
        this.rows = rows
        this.columns = columns
        values = Array(rows) { DoubleArray(columns) }
    }

    constructor(fileName: String) {
        val lines = Files.readAllLines(Paths.get(fileName))

        if (lines.isEmpty()) {
            throw IllegalArgumentException("Empty file!")
        }

        parseInputFile(lines)
    }

    companion object {
        fun onesMatrix(rows: Int, columns: Int): Matrix {
            val newMatrix = Matrix(rows, columns)
            for (i in 0 until rows) {
                newMatrix[i, i] = 1.0
            }

            return newMatrix
        }
    }

    fun getNumberOfRows() = rows

    fun getNumberOfColumns() = columns

    fun setValue(row: Int, column: Int, value: Double) {
        values[row][column] = value
    }

    fun getValue(row: Int, column: Int) = values[row][column]

    fun getColumn(column: Int): Matrix {
        val columnMatrix = Matrix(rows, 1)

        for (i in 0 until rows) {
            columnMatrix[i] = this[i, column]
        }

        return columnMatrix
    }

    fun forwardSubstitution(matrix: Matrix): Matrix {
        for (i in 0 until rows - 1) {
            for (j in i + 1 until rows) {
                matrix[j] -= this[j, i] * matrix[i]
            }
        }

        return matrix
    }

    fun backwardSubstitution(matrix: Matrix): Matrix {
        for (i in (rows - 1) downTo 0) {
            checkIfZero(this[i, i])
            matrix[i] /= this[i, i]
            for (j in 0 until i) {
                matrix[j] -= this[j, i] * matrix[i]
            }
        }

        return matrix
    }

    fun luDecomposition(): Matrix {
        for (i in 0 until rows - 1) {
            for (j in i + 1 until rows) {
                checkIfZero(this[i, i])
                this[j, i] /= this[i, i]

                for (k in i + 1 until rows) {
                    this[j, k] -= this[j, i] * this[i, k]
                }
            }
        }

        return this
    }

    fun lupDecomposition(resultMatrix: Matrix? = null): Matrix {
        for (i in 0 until rows - 1) {
            val pivotIndex = choosePivotElement(i)
            if (pivotIndex != i) {
                swapRows(i, pivotIndex, values)
                if (resultMatrix != null) {
                    swapRows(i, pivotIndex, resultMatrix.values)
                }
            }

            for (j in i + 1 until rows) {
                checkIfZero(this[i, i])
                this[j, i] /= this[i, i]

                for (k in i + 1 until rows) {
                    this[j, k] -= this[j, i] * this[i, k]
                }
            }
        }

        return this
    }

    fun transpose(): Matrix {
        val newMatrix = Matrix(columns, rows)

        for (i in 0 until rows) {
            for (j in 0 until columns) {
                newMatrix[j, i] = this[i, j]
            }
        }

        return newMatrix
    }

    fun inverse(): Matrix {
        if (rows != columns) {
            throw IllegalArgumentException("Cannot calculate inverse on non-square matrix.")
        }

        val matrixI = onesMatrix(rows, columns)
        lupDecomposition(matrixI)

        val newMatrix = Matrix(rows, columns)

        for (i in 0 until columns) {
            val column = matrixI.getColumn(i)

            val y = forwardSubstitution(column)
            val solution = backwardSubstitution(y)

            for (j in 0 until rows) {
                newMatrix[j, i] = solution[j]
            }
        }

        return newMatrix
    }

    private fun swapRows(from: Int, to: Int, values: Array<DoubleArray>) {
        for (i in 0 until values[0].size) {
            val temp = values[from][i]
            values[from][i] = values[to][i]
            values[to][i] = temp
        }
    }

    private fun choosePivotElement(currentIndex: Int): Int {
        var max = this[currentIndex, currentIndex].absoluteValue
        var pivot = currentIndex

        for (i in currentIndex + 1 until columns) {
            if (max < this[i, currentIndex].absoluteValue) {
                pivot = i
                max = this[i, currentIndex]
            }
        }

        return pivot
    }

    private fun addMatrix(matrix: Matrix): Matrix {
        return matrixValuesOperation(matrix) { a, b -> a + b }
    }

    private fun subMatrix(matrix: Matrix): Matrix {
        return matrixValuesOperation(matrix) { a, b -> a - b }
    }

    private fun multiplyMatrix(matrix: Matrix): Matrix {
        if (columns != matrix.rows) {
            throw IllegalArgumentException("Wrong dimensions!")
        }

        val newMatrix = Matrix(rows, matrix.columns)

        for (i in 0 until rows) {
            for (j in 0 until matrix.columns) {
                for (k in 0 until columns) {
                    newMatrix[i, j] += this[i, k] * matrix[k, j]
                }
            }
        }

        return newMatrix
    }

    private fun divideWithScalar(scalar: Double): Matrix {
        checkIfZero(scalar)

        return multiplyWithScalar(1.0 / scalar)
    }

    private fun multiplyWithScalar(scalar: Double): Matrix {
        val newMatrix = Matrix(rows, columns)

        for (i in 0 until rows) {
            for (j in 0 until columns) {
                newMatrix[i, j] = this[i, j] * scalar
            }
        }

        return newMatrix
    }

    private fun matrixValuesOperation(matrix: Matrix, operation: (Double, Double) -> Double): Matrix {
        if (matrix.rows != rows || matrix.columns != columns) {
            throw IllegalArgumentException("Wrong dimensions!")
        }

        val newMatrix = Matrix(rows, columns)

        for (i in 0 until rows) {
            for (j in 0 until columns) {
                newMatrix[i, j] = operation(this[i, j], matrix[i, j])
            }
        }

        return newMatrix
    }

    private fun checkIfZero(scalar: Double) {
        if (scalar.isZero()) {
            throw IllegalArgumentException("Cannot divide with zero.")
        }
    }

    private fun parseInputFile(lines: List<String>) {
        val firstLine = lines[0].split("\\s+".toRegex())

        rows = lines.size
        columns = firstLine.size

        values = Array(rows) { DoubleArray(columns) }

        lines.forEachIndexed { i, line ->
            val lineValues = line.split("\\s+".toRegex())

            lineValues.forEachIndexed { j, s ->
                values[i][j] = s.toDouble()
            }
        }
    }

    fun toFile(path: Path) {
        val writer = Files.newBufferedWriter(path)

        writer.use {
            values.forEachIndexed { index, valuesRow ->
                writer.write(Arrays.toString(valuesRow).replace("[", "").replace("]", "").trim())
                if (index != values.size - 1) writer.newLine()
            }

            writer.flush()
        }
    }

    override fun toString(): String {
        val arrayString = Arrays.deepToString(values).replace("], ", "]\n")
        return arrayString.substring(1, arrayString.length - 1)
    }


    operator fun plus(matrix: Matrix) = addMatrix(matrix)
    operator fun minus(matrix: Matrix) = subMatrix(matrix)
    operator fun times(matrix: Matrix) = multiplyMatrix(matrix)
    operator fun times(scalar: Double) = multiplyWithScalar(scalar)
    operator fun div(scalar: Double) = divideWithScalar(scalar)

    operator fun get(row: Int, column: Int = 0) = getValue(row, column)
    operator fun set(row: Int, column: Int, value: Double) = setValue(row, column, value)
    operator fun set(row: Int, value: Double) = setValue(row, 0, value)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Matrix

        if (!Arrays.deepEquals(values, other.values)) return false

        return true
    }

    override fun hashCode(): Int {
        return Arrays.deepHashCode(values)
    }
}