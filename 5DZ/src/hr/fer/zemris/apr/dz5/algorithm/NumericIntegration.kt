package hr.fer.zemris.apr.dz5.algorithm

import hr.fer.zemris.apr.dz5.data.Matrix

fun trapezoidalRule(
    A: Matrix,
    B: Matrix,
    x0: Matrix,
    T: Double,
    tMax: Double,
    strategy: (t: Double, matrix: Matrix) -> Unit = { _, _ -> }
): Matrix {
    val u = Matrix.onesMatrix(A.getNumberOfRows(), A.getNumberOfColumns())
    var xk = x0

    val firstOperand = (u - A * (T / 2.0)).inverse()
    val secondOperand = u + A * (T / 2.0)

    var t = T
    while (t <= tMax) {
        xk = firstOperand * secondOperand * xk + firstOperand * B
        strategy(t, xk)
        t += T
    }

    return xk
}

fun rungeKutta(
    A: Matrix,
    B: Matrix,
    x0: Matrix,
    T: Double,
    tMax: Double,
    strategy: (t: Double, matrix: Matrix) -> Unit = { _, _ -> }
): Matrix {
    var t = T
    var xk = x0

    while (t <= tMax) {
        val m1 = A * xk + B
        val m2 = A * (xk + (m1 * T / 2.0)) + B
        val m3 = A * (xk + (m2 * T / 2.0)) + B
        val m4 = A * (xk + (m3 * T)) + B

        xk += ((m1 + m2 * 2.0 + m3 * 2.0 + m4) * T / 6.0)
        strategy(t, xk)

        t += T
    }

    return xk
}