package hr.fer.zemris.apr.dz5

import hr.fer.zemris.apr.dz5.data.Matrix

fun main(args: Array<String>) {
    val A = Matrix("matrix4")
    val B = Matrix("matrixB")
    val startingX = Matrix("starting4")

    solveSystemAndShowResults(A, B, startingX, 0.1, 5.0)
    rungeKuttaStepsComparison(A, B, startingX, listOf(0.1, 0.05))
    rungeKuttaStepsComparison(A, B, startingX, listOf(0.03, 0.02, 0.01))
    rungeKuttaStepsComparison(A, B, startingX, listOf(0.02, 0.01, 0.005))
}