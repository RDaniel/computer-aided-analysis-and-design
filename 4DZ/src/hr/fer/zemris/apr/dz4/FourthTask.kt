package hr.fer.zemris.apr.dz4

import hr.fer.zemris.apr.dz4.algorithm.doubleGeneticAlgorithm
import hr.fer.zemris.apr.dz4.extensions.mean
import hr.fer.zemris.apr.dz4.extensions.median
import hr.fer.zemris.apr.dz4.extensions.numberOfSolutionsFound
import hr.fer.zemris.apr.dz4.extensions.transformValues
import hr.fer.zemris.apr.dz4.model.function.Schaffers6Function
import hr.fer.zemris.apr.dz4.model.solution.DoubleArraySolution


fun main(args: Array<String>) {

    val populationSize = 500
    val minimalAcceptableError = 10E-6
    val maxIterations = 1000000
    val mutationProbability = 0.05
    val maxEvaluations = 150000

    DoubleArraySolution.LOWER_BOUND = -25.0
    DoubleArraySolution.UPPER_BOUND = 25.0

    val mutationProbabilitySolutions: MutableMap<Double, MutableList<Double>> = HashMap()

    val function = Schaffers6Function(2)

    DoubleArraySolution.NUMBER_OF_VARIABLES = function.numberOfVariables

    val mutationProbabilities = listOf(0.1, 0.3, 0.6, 0.9)
    for (mutation in mutationProbabilities) {
        for (i in 0 until 5) {
            function.reset()
            val doubleSolution = doubleGeneticAlgorithm(function, populationSize, minimalAcceptableError, maxEvaluations, maxIterations, mutation, 3)

            mutationProbabilitySolutions.computeIfAbsent(mutation) { mutableListOf() }
            mutationProbabilitySolutions[mutation]?.add(doubleSolution.error)
        }
    }

    val populationSizeSolutions: MutableMap<Int, MutableList<Double>> = HashMap()

    val populationSizes = listOf(30, 50, 100, 200)
    for (popSize in populationSizes) {
        for (i in 0 until 5) {
            function.reset()
            val doubleSolution = doubleGeneticAlgorithm(function, popSize, minimalAcceptableError, maxEvaluations, maxIterations, mutationProbability, 3)

            populationSizeSolutions.computeIfAbsent(popSize) { mutableListOf() }
            populationSizeSolutions[popSize]?.add(doubleSolution.error)
        }
    }

    println()
    println("Optimal mutation probability searching")
    println("Solutions = $mutationProbabilitySolutions")
    println()
    println("Solutions found = ${mutationProbabilitySolutions.transformValues { it -> it.numberOfSolutionsFound(minimalAcceptableError) }}")
    println()
    println("Medians = ${mutationProbabilitySolutions.transformValues { it -> it.median() }}")
    println()
    println("Means = ${mutationProbabilitySolutions.transformValues { it -> it.mean() }}")

    println()
    println("Optimal population size searching")
    println("Solutions = $populationSizeSolutions")
    println()
    println("Solutions found = ${populationSizeSolutions.transformValues { it -> it.numberOfSolutionsFound(minimalAcceptableError) }}")
    println()
    println("Medians = ${populationSizeSolutions.transformValues { it -> it.median() }}")
    println()
    println("Means = ${populationSizeSolutions.transformValues { it -> it.mean() }}")
}