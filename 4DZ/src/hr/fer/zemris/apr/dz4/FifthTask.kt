package hr.fer.zemris.apr.dz4

import hr.fer.zemris.apr.dz4.algorithm.doubleGeneticAlgorithm
import hr.fer.zemris.apr.dz4.extensions.mean
import hr.fer.zemris.apr.dz4.extensions.median
import hr.fer.zemris.apr.dz4.extensions.numberOfSolutionsFound
import hr.fer.zemris.apr.dz4.extensions.transformValues
import hr.fer.zemris.apr.dz4.model.function.Schaffers6Function
import hr.fer.zemris.apr.dz4.model.solution.DoubleArraySolution


fun main(args: Array<String>) {
    val populationSize = 500
    val minimalAcceptableError = 10E-6
    val maxIterations = 1000000
    val mutationProbability = 0.05
    val maxEvaluations = 100000

    val function = Schaffers6Function(2)

    DoubleArraySolution.LOWER_BOUND = -25.0
    DoubleArraySolution.UPPER_BOUND = 25.0
    DoubleArraySolution.NUMBER_OF_VARIABLES = function.numberOfVariables

    val tournamentSizeSolutions: MutableMap<Int, MutableList<Double>> = HashMap()

    val tournamentSizes = listOf(3, 7, 20, 80)
    for (tournamentSize in tournamentSizes) {
        for (i in 0 until 5) {
            function.reset()
            val doubleSolution =
                doubleGeneticAlgorithm(function, populationSize, minimalAcceptableError, maxEvaluations, maxIterations, mutationProbability, tournamentSize)

            tournamentSizeSolutions.computeIfAbsent(tournamentSize) { mutableListOf() }
            tournamentSizeSolutions[tournamentSize]?.add(doubleSolution.error)
        }
    }

    println()
    println("Optimal k searching")
    println()
    println("Solutions found = ${tournamentSizeSolutions.transformValues { it -> it.numberOfSolutionsFound(minimalAcceptableError) }}")
    println()
    println("Medians = ${tournamentSizeSolutions.transformValues { it -> it.median() }}")
    println()
    println("Means = ${tournamentSizeSolutions.transformValues { it -> it.mean() }}")
}