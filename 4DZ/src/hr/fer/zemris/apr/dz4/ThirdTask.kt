package hr.fer.zemris.apr.dz4

import hr.fer.zemris.apr.dz4.algorithm.bitGeneticAlgorithm
import hr.fer.zemris.apr.dz4.algorithm.doubleGeneticAlgorithm
import hr.fer.zemris.apr.dz4.extensions.mean
import hr.fer.zemris.apr.dz4.extensions.median
import hr.fer.zemris.apr.dz4.extensions.numberOfSolutionsFound
import hr.fer.zemris.apr.dz4.extensions.transformInnerValues
import hr.fer.zemris.apr.dz4.model.function.AbstractFunction
import hr.fer.zemris.apr.dz4.model.function.Schaffers6Function
import hr.fer.zemris.apr.dz4.model.function.Schaffers7Function
import hr.fer.zemris.apr.dz4.model.solution.BitVectorSolution
import hr.fer.zemris.apr.dz4.model.solution.DoubleArraySolution


fun main(args: Array<String>) {
    val populationSize = 500
    val minimalAcceptableError = 10E-6
    val maxIterations = 1000000
    val maxEvaluations = 250000
    val mutationProbability = 0.05

    DoubleArraySolution.LOWER_BOUND = -25.0
    DoubleArraySolution.UPPER_BOUND = 25.0

    BitVectorSolution.LOWER_BOUND = -25
    BitVectorSolution.UPPER_BOUND = 25
    BitVectorSolution.PRECISION = 4

    val doubleSolutions: MutableMap<Int, MutableMap<String, MutableList<Double>>> = hashMapOf()
    val bitSolutions: MutableMap<Int, MutableMap<String, MutableList<Double>>> = hashMapOf()

    val dimensions = listOf(3, 6)
    for (dimension in dimensions) {
        BitVectorSolution.NUMBER_OF_VARIABLES = dimension
        DoubleArraySolution.NUMBER_OF_VARIABLES = dimension

        doubleSolutions.computeIfAbsent(dimension) { hashMapOf() }
        bitSolutions.computeIfAbsent(dimension) { hashMapOf() }

        var function: AbstractFunction = Schaffers6Function(dimension)
        computeDoubleResults(function, populationSize, minimalAcceptableError, maxEvaluations, maxIterations, mutationProbability, doubleSolutions, dimension)
        computeBitResults(function, populationSize, minimalAcceptableError, maxEvaluations, maxIterations, mutationProbability, bitSolutions, dimension)

        function = Schaffers7Function(dimension)
        computeDoubleResults(function, populationSize, minimalAcceptableError, maxEvaluations, maxIterations, mutationProbability, doubleSolutions, dimension)
        computeBitResults(function, populationSize, minimalAcceptableError, maxEvaluations, maxIterations, mutationProbability, bitSolutions, dimension)
    }

    println()
    println("Double representation results  = $doubleSolutions")
    println("Bit representation results  = $bitSolutions")
    println()
    println("Double solutions solutions found = ${doubleSolutions.transformInnerValues { it -> it.numberOfSolutionsFound(minimalAcceptableError) }}")
    println("Bit solutions solutions found = ${bitSolutions.transformInnerValues { it -> it.numberOfSolutionsFound(minimalAcceptableError) }}")
    println()
    println("Double representation medians = ${doubleSolutions.transformInnerValues { it -> it.median() }}")
    println("Bit representation medians = ${bitSolutions.transformInnerValues { it -> it.median() }}")
    println()
    println("Double representation means = ${doubleSolutions.transformInnerValues { it -> it.mean() }}")
    println("Bit representation means = ${bitSolutions.transformInnerValues { it -> it.mean() }}")
}

private fun computeDoubleResults(
    function: AbstractFunction,
    populationSize: Int,
    minimalAcceptableError: Double,
    maxEvaluations: Int,
    maxIterations: Int,
    mutationProbability: Double,
    doubleFunctionSolutions: MutableMap<Int, MutableMap<String, MutableList<Double>>>,
    dimension: Int
) {
    for (i in 0 until 5) {
        function.reset()
        val doubleSolution = doubleGeneticAlgorithm(function, populationSize, minimalAcceptableError, maxEvaluations, maxIterations, mutationProbability, 3)
        doubleFunctionSolutions[dimension]?.computeIfAbsent(function.getName()) { mutableListOf() }
        doubleFunctionSolutions[dimension]!![function.getName()]?.add(doubleSolution.error)
    }
}

private fun computeBitResults(
    function: AbstractFunction,
    populationSize: Int,
    minimalAcceptableError: Double,
    maxEvaluations: Int,
    maxIterations: Int,
    mutationProbability: Double,
    doubleFunctionSolutions: MutableMap<Int, MutableMap<String, MutableList<Double>>>,
    dimension: Int
) {
    for (i in 0 until 5) {
        function.reset()
        val bitSolution = bitGeneticAlgorithm(function, populationSize, minimalAcceptableError, maxEvaluations, maxIterations, mutationProbability)
        doubleFunctionSolutions[dimension]?.computeIfAbsent(function.getName()) { mutableListOf() }
        doubleFunctionSolutions[dimension]!![function.getName()]?.add(bitSolution.error)
    }
}
