package hr.fer.zemris.apr.dz4.model.population


import hr.fer.zemris.apr.dz4.model.solution.DoubleArraySolution
import java.util.*
import kotlin.collections.ArrayList

class DoubleArrayPopulation(var members: MutableList<DoubleArraySolution>) {

    private val random = Random()

    fun addMember(solution: DoubleArraySolution): DoubleArrayPopulation {
        members.add(solution)
        return this
    }

    fun removeMember(solution: DoubleArraySolution) {
        members.remove(solution)
    }

    fun size() = members.size

    fun getBestSolutions(numberOfBest: Int): List<DoubleArraySolution> {
        var bestCounter = numberOfBest
        val solutions = LinkedList<DoubleArraySolution>()
        var counter = 0
        while (bestCounter-- > 0) {
            solutions.add(members[counter++])
        }

        return solutions
    }

    fun getWorstSolutions(numberOfWorst: Int): List<DoubleArraySolution> {
        var worstCounter = numberOfWorst
        val solutions = LinkedList<DoubleArraySolution>()
        var counter = members.size - 1
        while (worstCounter-- > 0) {
            solutions.add(members[counter--])
        }

        return solutions
    }

    fun getRandomSolutions(numberOfSolutions: Int): List<DoubleArraySolution> {
        var solutionCounter = numberOfSolutions
        val solutions = LinkedList<DoubleArraySolution>()

        val tempArray = ArrayList<DoubleArraySolution>(members)
        while (solutionCounter-- > 0) {
            val randomIndex = random.nextInt(tempArray.size)
            solutions.add(tempArray[randomIndex])
            tempArray.removeAt(randomIndex)
        }

        return solutions
    }

    fun evaluate() {
        members.sortWith(Comparator.comparingDouble { sol -> sol.error })
    }
}