package hr.fer.zemris.apr.dz4.model.population


import hr.fer.zemris.apr.dz4.model.solution.BitVectorSolution
import java.util.*

class BitVectorPopulation(var members: MutableList<BitVectorSolution>) {

    private val random = Random()

    fun addMember(solution: BitVectorSolution): BitVectorPopulation {
        members.add(solution)
        evaluate()
        return this
    }

    fun removeMember(solution: BitVectorSolution) {
        members.remove(solution)
    }

    fun size() = members.size

    fun getBestSolutions(numberOfBest: Int): List<BitVectorSolution> {
        var bestCounter = numberOfBest
        val solutions = LinkedList<BitVectorSolution>()
        var counter = 0
        while (bestCounter-- > 0) {
            solutions.add(members[counter++])
        }

        return solutions
    }

    fun getWorstSolutions(numberOfWorst: Int): List<BitVectorSolution> {
        var worstCounter = numberOfWorst
        val solutions = LinkedList<BitVectorSolution>()
        var counter = members.size - 1
        while (worstCounter-- > 0) {
            solutions.add(members[counter--])
        }

        return solutions
    }

    fun getRandomSolutions(numberOfBest: Int): List<BitVectorSolution> {
        var randomCounter = numberOfBest
        val solutions = LinkedList<BitVectorSolution>()

        val tempArray = ArrayList<BitVectorSolution>(members)
        while (randomCounter-- > 0) {
            val randomIndex = random.nextInt(tempArray.size)
            solutions.add(tempArray[randomIndex])
            tempArray.removeAt(randomIndex)
        }

        return solutions
    }

    fun evaluate() {
        members.sortWith(Comparator.comparingDouble { it -> it.error })
    }
}