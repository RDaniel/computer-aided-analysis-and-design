package hr.fer.zemris.apr.dz4.model.function

class Schaffers6Function(n: Int) : AbstractFunction() {

    override var startingPoint = Array(n) { 5.0 }

    override val minimum = 0.0

    override val numberOfVariables: Int = n

    override fun calculate(x: Array<Double>): Double {
        val squaredSum = x.map { it * it }.sum()
        return 0.5 + (Math.pow(Math.sin(Math.sqrt(squaredSum)), 2.0) - 0.5) / Math.pow((1 + 0.001 * squaredSum), 2.0)
    }

    override fun getName() = "Schaffers 6 function"
}