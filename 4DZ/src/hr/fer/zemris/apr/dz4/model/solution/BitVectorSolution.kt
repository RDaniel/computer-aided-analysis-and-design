package hr.fer.zemris.apr.dz4.model.solution

import java.util.*
import kotlin.math.log10

class BitVectorSolution {

    companion object {
        var LOWER_BOUND = -1
        var UPPER_BOUND = 1
        var PRECISION = 2
        var NUMBER_OF_VARIABLES = 2
        var SOLUTION_SIZE = 8
        var BITS_PER_VARIABLE = 4
    }

    lateinit var values: BitSet

    private val random = Random()

    var error = 0.0

    constructor() {
        createRandomValues()
    }

    constructor(bitSet: BitSet) {
        this.values = bitSet
    }

    fun decode(): Array<Double> {
        val solutionArray = Array(NUMBER_OF_VARIABLES) { 0.0 }

        var position = 0
        var currentValue = 0
        var index = 0
        for (i in 0 until SOLUTION_SIZE) {
            if (values.get(i)) {
                currentValue += Math.pow(2.0, position.toDouble()).toInt()
            }

            position++

            if ((i + 1) % BITS_PER_VARIABLE == 0) {
                solutionArray[index++] = LOWER_BOUND + currentValue / (Math.pow(2.0, BITS_PER_VARIABLE.toDouble()) - 1) * (UPPER_BOUND - LOWER_BOUND)
                position = 0
                currentValue = 0
            }
        }

        return solutionArray
    }

    private fun createRandomValues() {
        SOLUTION_SIZE = calculateNumberOfBits()
        values = BitSet(SOLUTION_SIZE)

        for (i in 0 until SOLUTION_SIZE) {
            values.set(i, random.nextBoolean())
        }
    }

    private fun calculateNumberOfBits(): Int {
        val upperLog = log10(1 + (UPPER_BOUND - LOWER_BOUND) * Math.pow(10.toDouble(), PRECISION.toDouble()))
        BITS_PER_VARIABLE = Math.floor(upperLog / log10(2.toDouble())).toInt()
        return BITS_PER_VARIABLE * NUMBER_OF_VARIABLES
    }

    override fun toString(): String {
        return values.toString()
    }
}