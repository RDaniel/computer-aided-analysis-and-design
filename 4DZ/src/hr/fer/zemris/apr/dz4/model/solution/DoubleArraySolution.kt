package hr.fer.zemris.apr.dz4.model.solution

import java.util.*

class DoubleArraySolution {

    companion object {
        var LOWER_BOUND = -1.0
        var UPPER_BOUND = 1.0
        var NUMBER_OF_VARIABLES = 2
    }

    lateinit var values: Array<Double>

    private val random = Random()

    var error = 0.0

    constructor(values: Array<Double>) {
        this.values = values
    }

    constructor() {
        createRandomValues()
    }

    private fun createRandomValues() {
        values = Array(NUMBER_OF_VARIABLES) { 0.0 }

        for (i in 0 until NUMBER_OF_VARIABLES) {
            values[i] = LOWER_BOUND + random.nextDouble() * (UPPER_BOUND - LOWER_BOUND)
        }
    }

    override fun toString(): String {
        return Arrays.toString(values)
    }
}