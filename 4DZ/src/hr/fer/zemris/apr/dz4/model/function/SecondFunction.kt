package hr.fer.zemris.apr.dz4.model.function

class SecondFunction(n: Int) : AbstractFunction() {

    override var startingPoint = Array(n) { 0.0 }

    override val minimum: Double = 0.0

    override val numberOfVariables: Int = n

    override fun calculate(x: Array<Double>) = x.mapIndexed { index, d -> (d - index - 1) * (d - index - 1) }.sum()

    override fun getName() = "Second function"
}