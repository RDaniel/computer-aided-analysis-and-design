package hr.fer.zemris.apr.dz4.model.function

class Schaffers7Function(n: Int) : AbstractFunction() {

    override var startingPoint = Array(n) { 5.0 }

    override val minimum = 0.0

    override val numberOfVariables: Int = n

    override fun calculate(x: Array<Double>): Double {
        val squaredSum = x.map { it * it }.sum()
        return Math.pow(squaredSum, 0.25) * (1 + Math.pow(Math.sin(50 * Math.pow(squaredSum, 0.1)), 2.0))
    }

    override fun getName() = "Schaffers 7 function"
}