package hr.fer.zemris.apr.dz4.algorithm

import hr.fer.zemris.apr.dz4.geneticUtils.*
import hr.fer.zemris.apr.dz4.model.function.AbstractFunction
import hr.fer.zemris.apr.dz4.model.population.BitVectorPopulation
import hr.fer.zemris.apr.dz4.model.population.DoubleArrayPopulation
import hr.fer.zemris.apr.dz4.model.solution.BitVectorSolution
import hr.fer.zemris.apr.dz4.model.solution.DoubleArraySolution
import java.util.*

fun doubleGeneticAlgorithm(
    function: AbstractFunction,
    popSize: Int,
    acceptableError: Double,
    maxNumberOfFunctionEvaluations: Int,
    maxIterations: Int,
    mutationProbability: Double,
    tournamentSize: Int = 3
): DoubleArraySolution {
    var numberOfIterations = maxIterations

    val population = createDoublePopulation(function, popSize)
    population.evaluate()

    var bestSolution: DoubleArraySolution = population.getBestSolutions(1)[0]

    while (function.numberOfInvocations < maxNumberOfFunctionEvaluations && numberOfIterations-- > 0) {
        println(function.evaluate(bestSolution.values))
        if (function.evaluate(bestSolution.values) < acceptableError) {
            break
        }

        val firstParent = tournamentSelection(population, tournamentSize)
        val secondParent = tournamentSelection(population, tournamentSize)

        val child = blxAlphaCrossover(firstParent, secondParent)
        normalDistributionMutator(child, mutationProbability)
        child.error = function.evaluate(child.values)

        val solutionToReplace = population.getWorstSolutions(3).random()
        population.removeMember(solutionToReplace)
        population.addMember(child)

        population.evaluate()
        bestSolution = population.getBestSolutions(1)[0]
    }

    return bestSolution
}

fun bitGeneticAlgorithm(
    function: AbstractFunction,
    popSize: Int,
    acceptableError: Double,
    maxNumberOfFunctionEvaluations: Int,
    maxIterations: Int,
    mutationProbability: Double
): BitVectorSolution {
    var numberOfIterations = maxIterations

    val population = createBitPopulation(function, popSize)
    population.evaluate()

    var bestSolution = population.getBestSolutions(1)[0]

    while (function.numberOfInvocations < maxNumberOfFunctionEvaluations && numberOfIterations-- > 0) {
        println(function.evaluate(bestSolution.decode()))
        if (function.evaluate(bestSolution.decode()) < acceptableError) {
            break
        }

        val firstParent = tournamentSelection(population, 3)
        val secondParent = tournamentSelection(population, 3)

        val child = onePointCrossover(firstParent, secondParent)
        uniformMutation(child, mutationProbability)
        child.error = function.evaluate(child.decode())

        val solutionToReplace = population.getWorstSolutions(3).random()
        population.removeMember(solutionToReplace)
        population.addMember(child)

        population.evaluate()
        bestSolution = population.getBestSolutions(1)[0]
    }

    return bestSolution
}

private fun createBitPopulation(function: AbstractFunction, populationSize: Int): BitVectorPopulation {
    var solutionsToAdd = populationSize
    val members = ArrayList<BitVectorSolution>()
    while (solutionsToAdd-- > 0) {
        val solution = BitVectorSolution()
        solution.error = function.evaluate(solution.decode())
        members.add(solution)
    }

    return BitVectorPopulation(members)
}

private fun createDoublePopulation(function: AbstractFunction, populationSize: Int): DoubleArrayPopulation {
    var solutionsToAdd = populationSize
    val members = ArrayList<DoubleArraySolution>()
    while (solutionsToAdd-- > 0) {
        val solution = DoubleArraySolution()
        solution.error = function.evaluate(solution.values)
        members.add(solution)
    }

    return DoubleArrayPopulation(members)
}
