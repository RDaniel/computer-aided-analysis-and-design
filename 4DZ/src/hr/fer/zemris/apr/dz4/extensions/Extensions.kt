package hr.fer.zemris.apr.dz4.extensions

import kotlin.streams.toList


fun List<Double>.median(): Double {
    val sortedList = sorted()

    return if (sortedList.size % 2 == 0) {
        (sortedList[sortedList.size / 2] + sortedList[sortedList.size / 2 - 1])
    } else {
        sortedList[sortedList.size / 2]
    }
}

fun List<Double>.mean(): Double {
    return stream().mapToDouble { it }.average().asDouble
}

fun List<Double>.min(): Double {
    return stream().mapToDouble { it }.min().asDouble
}

fun List<Double>.max(): Double {
    return stream().mapToDouble { it }.max().asDouble
}

fun List<Double>.numberOfSolutionsFound(border: Double): Double {
    return stream().filter { it < border }.toList().size.toDouble()
}

fun <T> Map<T, List<Double>>.transformValues(function: (List<Double>) -> Double): Map<T, Double> {
    return mapValues { it -> function(it.value) }
}

fun Map<Int, Map<String, MutableList<Double>>>.transformInnerValues(function: (List<Double>) -> Double): Map<Int, Map<String, Double>> {
    return mapValues { it -> it.value.mapValues { innerMap -> function(innerMap.value) } }
}