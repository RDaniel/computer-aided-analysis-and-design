package hr.fer.zemris.apr.dz4.geneticUtils

import hr.fer.zemris.apr.dz4.model.solution.BitVectorSolution
import hr.fer.zemris.apr.dz4.model.solution.DoubleArraySolution
import java.util.*


const val BLX_ALPHA = 0.05

fun blxAlphaCrossover(first: DoubleArraySolution, second: DoubleArraySolution): DoubleArraySolution {
    val firstValues = first.values
    val secondValues = second.values

    val newValues = Array(firstValues.size) { 0.0 }
    val random = Random()
    for (i in firstValues.indices) {
        var min = if (firstValues[i] <= secondValues[i]) firstValues[i] else secondValues[i]
        var max = if (firstValues[i] > secondValues[i]) firstValues[i] else secondValues[i]
        var range = max - min
        min -= range * BLX_ALPHA
        max += range * BLX_ALPHA
        range = max - min
        newValues[i] = min + range * random.nextDouble()

        if (newValues[i] > DoubleArraySolution.UPPER_BOUND) {
            newValues[i] = DoubleArraySolution.UPPER_BOUND
        } else if (newValues[i] < DoubleArraySolution.LOWER_BOUND) {
            newValues[i] = DoubleArraySolution.LOWER_BOUND
        }
    }

    return DoubleArraySolution(newValues)
}

private const val ALPHA = 0.4

fun arithmeticCrossover(first: DoubleArraySolution, second: DoubleArraySolution): DoubleArraySolution {
    val firstValues = first.values
    val secondValues = second.values

    val newValues = Array(firstValues.size) { 0.0 }
    for (i in firstValues.indices) {
        newValues[i] = ALPHA * firstValues[i] + (1 - ALPHA) * secondValues[i]
    }

    return DoubleArraySolution(newValues)
}

fun onePointCrossover(firstParent: BitVectorSolution, secondParent: BitVectorSolution): BitVectorSolution {
    val first = firstParent.values
    val second = secondParent.values

    val random = Random()

    val numberOfBits = BitVectorSolution.SOLUTION_SIZE
    val newBitSet = BitSet(numberOfBits)
    val crossoverPoint = random.nextInt(numberOfBits)

    for (i in 0 until numberOfBits) {
        if (i < crossoverPoint) {
            newBitSet.set(i, first.get(i))
        } else {
            newBitSet.set(i, second.get(i))
        }
    }

    return BitVectorSolution(newBitSet)
}


fun uniformCrossover(firstParent: BitVectorSolution, secondParent: BitVectorSolution): BitVectorSolution {
    val first = firstParent.values
    val second = secondParent.values

    val random = Random()

    val numberOfBits = BitVectorSolution.SOLUTION_SIZE
    val newBitSet = BitSet(numberOfBits)

    for (i in 0 until numberOfBits) {
        val p1 = first.get(i)
        val p2 = second.get(i)
        newBitSet.set(i, (p1 and p2) or (random.nextBoolean() and p1 xor p2))
    }

    return BitVectorSolution(newBitSet)
}
