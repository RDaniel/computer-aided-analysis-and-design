package hr.fer.zemris.apr.dz4.geneticUtils

import hr.fer.zemris.apr.dz4.model.solution.BitVectorSolution
import hr.fer.zemris.apr.dz4.model.solution.DoubleArraySolution
import java.util.*


private const val SIGMA = 0.3

fun normalDistributionMutator(solution: DoubleArraySolution, mutationProbability: Double) {
    val values = solution.values
    val random = Random()
    for (i in values.indices) {
        if (random.nextDouble() < mutationProbability) {
            values[i] += random.nextGaussian() * SIGMA

            if (values[i] > DoubleArraySolution.UPPER_BOUND) {
                values[i] = DoubleArraySolution.UPPER_BOUND
            } else if (values[i] < DoubleArraySolution.LOWER_BOUND) {
                values[i] = DoubleArraySolution.LOWER_BOUND
            }
        }
    }
}

fun uniformMutation(solution: BitVectorSolution, mutationProbability: Double) {
    val random = Random()
    val bitSet = solution.values
    for (i in 0 until BitVectorSolution.SOLUTION_SIZE) {
        if (random.nextDouble() < mutationProbability) {
            bitSet.set(i, !bitSet.get(i))
        }
    }
}
