package hr.fer.zemris.apr.dz4.geneticUtils


import hr.fer.zemris.apr.dz4.model.function.AbstractFunction
import hr.fer.zemris.apr.dz4.model.population.BitVectorPopulation
import hr.fer.zemris.apr.dz4.model.population.DoubleArrayPopulation
import hr.fer.zemris.apr.dz4.model.solution.BitVectorSolution
import hr.fer.zemris.apr.dz4.model.solution.DoubleArraySolution
import java.util.*


fun tournamentSelection(population: DoubleArrayPopulation, k: Int): DoubleArraySolution {
    val tournamentPopulation = DoubleArrayPopulation(population.getRandomSolutions(k).toMutableList())
    tournamentPopulation.evaluate()
    return tournamentPopulation.getBestSolutions(1)[0]
}

fun tournamentSelection(population: BitVectorPopulation, k: Int): BitVectorSolution {
    val tournamentPopulation = BitVectorPopulation(population.getRandomSolutions(k).toMutableList())
    tournamentPopulation.evaluate()
    return tournamentPopulation.getBestSolutions(1)[0]
}


fun rouletteSelection(function: AbstractFunction, population: DoubleArrayPopulation): DoubleArraySolution? {
    val members = population.members
    val fitnessMap = TreeMap<Double, DoubleArraySolution>()
    val rangeHolder = HashMap<DoubleArraySolution, RangeHolder>()

    var totalCount = 0.0
    val size = (members.size - 1).toDouble()
    var counter = 0.0
    for (member in members) {
        val fitness = size - counter++
        fitnessMap[fitness] = member
        totalCount += fitness
    }

    var leftBorder = 0.0
    for ((key, value) in fitnessMap) {
        val range = key / totalCount
        rangeHolder[value] = RangeHolder(leftBorder, leftBorder + range)
        leftBorder += range
    }

    val random = Random()
    var solution: DoubleArraySolution? = null
    val first = random.nextDouble()
    for ((key, value) in rangeHolder) {
        if (value.isIn(first)) {
            solution = key
        }
    }

    return solution
}


private class RangeHolder(private val leftBorder: Double, private val rightBorder: Double) {

    fun isIn(value: Double): Boolean {
        return value >= leftBorder && value < rightBorder
    }
}