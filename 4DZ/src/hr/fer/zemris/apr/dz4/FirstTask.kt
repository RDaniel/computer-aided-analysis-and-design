package hr.fer.zemris.apr.dz4

import hr.fer.zemris.apr.dz4.algorithm.bitGeneticAlgorithm
import hr.fer.zemris.apr.dz4.algorithm.doubleGeneticAlgorithm
import hr.fer.zemris.apr.dz4.extensions.*
import hr.fer.zemris.apr.dz4.model.function.RosenbrockFunction
import hr.fer.zemris.apr.dz4.model.function.Schaffers6Function
import hr.fer.zemris.apr.dz4.model.function.Schaffers7Function
import hr.fer.zemris.apr.dz4.model.function.SecondFunction
import hr.fer.zemris.apr.dz4.model.solution.BitVectorSolution
import hr.fer.zemris.apr.dz4.model.solution.DoubleArraySolution


fun main(args: Array<String>) {
    val functions = arrayOf(
        RosenbrockFunction(),
        SecondFunction(5),
        Schaffers6Function(2),
        Schaffers7Function(2)
    )

    val populationSize = 500
    val minimalAcceptableError = 10E-6
    val maxIterations = 1000000
    val maxEvaluations = 300000
    val mutationProbability = 0.05

    BitVectorSolution.LOWER_BOUND = -50
    BitVectorSolution.UPPER_BOUND = 100
    BitVectorSolution.PRECISION = 4

    DoubleArraySolution.LOWER_BOUND = -50.0
    DoubleArraySolution.UPPER_BOUND = 100.0

    val bitFunctionSolutions: MutableMap<String, MutableList<Double>> = HashMap()
    val doubleFunctionSolutions: MutableMap<String, MutableList<Double>> = HashMap()

    for (function in functions) {
        BitVectorSolution.NUMBER_OF_VARIABLES = function.numberOfVariables
        DoubleArraySolution.NUMBER_OF_VARIABLES = function.numberOfVariables

        for (i in 0 until 10) {
            function.reset()
            val bitSolution = bitGeneticAlgorithm(function, populationSize, minimalAcceptableError, maxEvaluations, maxIterations, mutationProbability)

            function.reset()
            val doubleSolution = doubleGeneticAlgorithm(function, populationSize, minimalAcceptableError, maxEvaluations, maxIterations, mutationProbability, 3)

            bitFunctionSolutions.computeIfAbsent(function.getName()) { mutableListOf() }
            bitFunctionSolutions[function.getName()]?.add(bitSolution.error)

            doubleFunctionSolutions.computeIfAbsent(function.getName()) { mutableListOf() }
            doubleFunctionSolutions[function.getName()]?.add(doubleSolution.error)
        }
    }

    println()
    println("Bit representation results = $bitFunctionSolutions")
    println("Double representation results  = $doubleFunctionSolutions")
    println()
    println("Bit representation solutions found = ${bitFunctionSolutions.transformValues { it -> it.numberOfSolutionsFound(minimalAcceptableError) }}")
    println("Double solutions solutions found = ${doubleFunctionSolutions.transformValues { it -> it.numberOfSolutionsFound(minimalAcceptableError) }}")
    println()
    println("Bit representation medians = ${bitFunctionSolutions.transformValues { it -> it.median() }}")
    println("Double representation medians = ${doubleFunctionSolutions.transformValues { it -> it.median() }}")
    println()
    println("Bit representation means = ${bitFunctionSolutions.transformValues { it -> it.mean() }}")
    println("Double representation means = ${doubleFunctionSolutions.transformValues { it -> it.mean() }}")
    println()
    println("Bit representation min = ${bitFunctionSolutions.transformValues { it -> it.min() }}")
    println("Double representation min = ${doubleFunctionSolutions.transformValues { it -> it.min() }}")
    println()
    println("Bit representation max = ${bitFunctionSolutions.transformValues { it -> it.max() }}")
    println("Double representation max = ${doubleFunctionSolutions.transformValues { it -> it.max() }}")
}