package hr.fer.zemris.apr.dz4

import hr.fer.zemris.apr.dz4.algorithm.doubleGeneticAlgorithm
import hr.fer.zemris.apr.dz4.extensions.mean
import hr.fer.zemris.apr.dz4.extensions.median
import hr.fer.zemris.apr.dz4.extensions.numberOfSolutionsFound
import hr.fer.zemris.apr.dz4.extensions.transformInnerValues
import hr.fer.zemris.apr.dz4.model.function.AbstractFunction
import hr.fer.zemris.apr.dz4.model.function.Schaffers6Function
import hr.fer.zemris.apr.dz4.model.function.Schaffers7Function
import hr.fer.zemris.apr.dz4.model.solution.BitVectorSolution
import hr.fer.zemris.apr.dz4.model.solution.DoubleArraySolution


fun main(args: Array<String>) {
    val populationSize = 500
    val minimalAcceptableError = 10E-6
    val maxIterations = 1000000
    val maxEvaluations = 350000
    val mutationProbability = 0.2

    DoubleArraySolution.LOWER_BOUND = -50.0
    DoubleArraySolution.UPPER_BOUND = 100.0

    val doubleFunctionSolutions: MutableMap<Int, MutableMap<String, MutableList<Double>>> = hashMapOf()

    val dimensions = listOf(1, 3, 6, 10)
    for (dimension in dimensions) {
        BitVectorSolution.NUMBER_OF_VARIABLES = dimension
        DoubleArraySolution.NUMBER_OF_VARIABLES = dimension

        var function: AbstractFunction = Schaffers6Function(dimension)

        doubleFunctionSolutions.computeIfAbsent(dimension) { hashMapOf() }

        for (i in 0 until 10) {
            val doubleSolution = doubleGeneticAlgorithm(function, populationSize, minimalAcceptableError, maxEvaluations, maxIterations, mutationProbability, 3)
            doubleFunctionSolutions[dimension]?.computeIfAbsent(function.getName()) { mutableListOf() }
            doubleFunctionSolutions[dimension]!![function.getName()]?.add(doubleSolution.error)
        }

        function = Schaffers7Function(dimension)
        for (i in 0 until 10) {

            val doubleSolution = doubleGeneticAlgorithm(function, populationSize, minimalAcceptableError, maxEvaluations, maxIterations, mutationProbability, 3)
            doubleFunctionSolutions[dimension]?.computeIfAbsent(function.getName()) { mutableListOf() }
            doubleFunctionSolutions[dimension]!![function.getName()]?.add(doubleSolution.error)
        }
    }

    println()
    println("Double representation results  = $doubleFunctionSolutions")
    println()
    println("Double solutions solutions found = ${doubleFunctionSolutions.transformInnerValues { it -> it.numberOfSolutionsFound(minimalAcceptableError) }}")
    println()
    println("Double representation medians = ${doubleFunctionSolutions.transformInnerValues { it -> it.median() }}")
    println()
    println("Double representation means = ${doubleFunctionSolutions.transformInnerValues { it -> it.mean() }}")
}
