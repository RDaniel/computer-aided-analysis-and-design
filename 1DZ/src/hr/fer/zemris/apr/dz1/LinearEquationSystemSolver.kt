package hr.fer.zemris.apr.dz1

/**
 * 2a*x = b -> LU decomposition to convert 2a to L and U
 * L * U * x = b
 * y = U * x
 * L * y = b -> forward substitution to get y
 * y = U * x -> backward substitution to get x
 */
fun solveEquationUsingLU(systemMatrix: Matrix, solutionVector: Matrix): Matrix {
    systemMatrix.luDecomposition()

    val y = systemMatrix.forwardSubstitution(solutionVector)

    return systemMatrix.backwardSubstitution(y)
}

fun solveEquationUsingLUP(systemMatrix: Matrix, solutionVector: Matrix): Matrix {
    systemMatrix.lupDecomposition(solutionVector)

    val y = systemMatrix.forwardSubstitution(solutionVector)

    return systemMatrix.backwardSubstitution(y)
}