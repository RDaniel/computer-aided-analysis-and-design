package hr.fer.zemris.apr.dz1

fun main(args: Array<String>) {
    for (i in 1..5) {
        println("$i. task")

        var systemMatrix = Matrix("${i}a")
        var resultMatrix = Matrix("${i}y")
        printEquationSystemLUResult(systemMatrix, resultMatrix)

        systemMatrix = Matrix("${i}a")
        resultMatrix = Matrix("${i}y")
        printEquationSystemLUPResult(systemMatrix, resultMatrix)

        println()
    }
}

fun printEquationSystemLUResult(systemMatrix: Matrix, resultMatrix: Matrix) {
    println("LU")
    try {
        println(solveEquationUsingLU(systemMatrix, resultMatrix))
    } catch (ex: Exception) {
        println(ex.message)
    }
}

fun printEquationSystemLUPResult(systemMatrix: Matrix, resultMatrix: Matrix) {
    println("LUP")
    try {
        println(solveEquationUsingLUP(systemMatrix, resultMatrix))
    } catch (ex: Exception) {
        println(ex.message)
    }
}